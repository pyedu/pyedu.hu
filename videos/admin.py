from django.contrib import admin
from videos.models import Video
from exercises.models import Choice, Answer


class AnswerInline(admin.TabularInline):
    model = Answer
    extra = 2


class ChoiceAdmin(admin.ModelAdmin):
    inlines = [AnswerInline]

admin.site.register(Video)
admin.site.register(Choice, ChoiceAdmin)
