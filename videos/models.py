from django.db import models
from courses.models import Unit


class Video(models.Model):
    unit = models.ForeignKey(Unit)
    file_name = models.CharField(max_length=200)
    url = models.CharField(max_length=500)
    V_TYPE= (
            ('nk', 'newKnowledge'),
            ('qs', 'question'),
            ('so', 'solution'),
            )
    type = models.CharField(max_length=2,
            choices=V_TYPE)

    def __str__(self):
        return self.file_name

    def save(self, *args, **kwargs):
        for video in self.unit.video_set.all():
            if video.type == self.type:
                raise ValueError('The videos of a unit must have different types.')
        super().save(*args, **kwargs)
