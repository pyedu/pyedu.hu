from django.shortcuts import render, redirect
# from django.contrib.auth.decorators import login_required

from courses.models import Course
import courses
from exercises.views import exercise
from pyEdu import settings


def video_display(request, video, next_item):
    return render(
        request,
        'videos/index.html',
        {
            'unit': video.unit,
            'next_item': next_item,
            'video': video,
        }
    )


def unit(request, course_id, lesson_serial, unit_serial):
    course = Course.objects.get(pk=course_id)
    unit = course.get_element(int(lesson_serial), int(unit_serial))
    item_type = unit.first_item().type
    return redirect("item", course_id, lesson_serial, unit_serial, item_type)


def item(request, course_id, lesson_serial, unit_serial, type):
    course = Course.objects.get(pk=course_id)
    item = course.get_element(int(lesson_serial), int(unit_serial), type)
    if not request.user.is_authenticated() and not item.unit.is_public():
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))
    unit = item.unit
    next_item = courses.models.next_item(item)
    if type == "ex":
        return exercise(request, unit)
    elif type in ("nk", "qs", "so"):
        return video_display(request, item, next_item)
