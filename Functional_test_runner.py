#!/usr/bin/env python3

import argparse
import subprocess

DEV_SERVER = "pyedu-dev.amk.uni-obuda.hu"
LOCAL_SERVER = "pyedu-dev.amk.uni-obuda.hu"

parser = argparse.ArgumentParser(
    description='Runs functional tests'
    ' on a given of the default server')
parser.add_argument('server',
                    nargs="?",
                    help='The name of the server',
                    default=DEV_SERVER)
args = parser.parse_args()


def get_server_name(given_server_name):
    if args.server in ["local", "l"]:
        return "127.0.0.1:8000"
    else:
        return args.server

command = [
    "manage.py", "test", "functional_tests",
    "--liveserver={server}".format(server=get_server_name(args.server))
]
subprocess.call(command)
