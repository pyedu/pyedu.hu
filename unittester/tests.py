import unittest
import sys
sys.path.append('.')
import unittester
from unittester import main
import os

erroneous_codes = [
        """
 x=1
        """,
        """
else = 7
        """,
        """
x = 1/0
        """,
        """
x = valami + 1
        """,
]

# code, failure, error
codes = [
    (
        """
from math import sqrt

def diszkriminans(a, b, c):
    return b*b - 4*a*c

def masodfoku(*egyutthatok):
    assert len(egyutthatok) == 3
    D = diszkriminans(*egyutthatok)
    a, b, c = egyutthatok
    x1 = (-b - sqrt(D)) / (2*a)
    x2 = (-b + sqrt(D)) / (2*a)
    return x1, x2
        """,
        0, 0
    ),
    (
        """
from math import sqrt

def diszkriminans(a, b, c):
    return b*b - 4*a*c

def masodfoku(*egyutthatok):
    a, b, c = egyutthatok
    D = diszkriminans(*egyutthatok)
    x1 = (-b - sqrt(D)) / (2*a)
    x2 = (-b + sqrt(D)) / (2*a)
    return x1, x2
""",
        0, 1
    ),
    (
        """
from math import sqrt

def diszkriminans(a, b, c):
    return b*b - 4*a*c

def masodfoku(*egyutthatok):
    a, b, c = egyutthatok
    D = diszkriminans(*egyutthatok)
    x1 = (-b - sqrt(D)) / (2*a)
    x2 = (-b + sqrt(D)) / (1*a)
    return x1, x2
""",
        1, 1
    ),
    (
        """
from math import sqrt

def diszkriminans(a, b, c):
    return b*b - 4*a*c

def masodfoku(*egyutthatok):
    assert len(egyutthatok) == 3
    D = diszkriminans(*egyutthatok)
    a, b, c = egyutthatok
    x1 = (-b - sqrt(D)) / (2*a)
    x2 = (-b + sqrt(D)) / (1*a)
    return x1, x2
        """,
        1, 0
    ),
    (
        """
from math import sqrt

def diszkriminans(a, b, c):
    return b*b - 4*a*c
        """,
        0, 2
    ),
    (
        """
from math import sqrt

def diszkriminans(a, b, c):
    return b
        """,
        1, 2
    ),
    (
        """
from math import sqrt

def diszkriminans(a, b, c):
    return b*b - 4*a*c

def masodfoku(*args):
        pass
        """,
        1, 1
    ),
]

unittest_code = """
import unittest


known_values = (
    ((1, 2, 1), (-1, -1), 0),
    ((1, -2, 1), (1, 1), 0),
    ((1, -1, -6), (3, -2), 25),
    ((1, -5, 6), (3, 2), 1),
)


class MasodfokuTest(unittest.TestCase):

    def test_masodfoku_returns_proper_values(self):
        "A masodfoku függvénynek helyes értékekkel kell visszatérnie."
        for args, return_values, _ in known_values:
            self.assertEqual(set(masodfoku(*args)), set(return_values))

    def test_masodfoku_raise_exception_if_number_of_args_not_3(self):
        "A masodfoku függvény kivételt dob, ha nem három argumentummal hívják."
        bad_args = ((1, 2, 1, 1), (1,), (1, 2))
        for args in bad_args:
            with self.assertRaises(AssertionError):
                masodfoku(*args)


class DiszkriminansTest(unittest.TestCase):

    def test_diszkriminans_returns_proper_values(self):
        "A diszkriminans függvénynek helyes értékekkel kell visszatérnie."
        for args, _, return_values in known_values:
            self.assertEqual(diszkriminans(*args), return_values)


test_cases = [MasodfokuTest, DiszkriminansTest]
"""


class UnittestTest(unittest.TestCase):
    def test_testcase_statistics_and_messages_returns_with_proper_values(self):
        sys.path.append('/tmp')
        for code, expected_number_of_failures, expected_number_of_errors in codes:
            module_name = unittester.save_code_and_unittest(code, unittest_code)
            exec('import {}'.format(module_name))
            test_case = eval('{}.test_cases'.format(module_name))
            stat_dict = unittester.testcase_statistics_and_messages(*test_case)
            number_of_errors = stat_dict["number_of_errors"]
            number_of_failures = stat_dict["number_of_failures"]
            self.assertEqual(
                expected_number_of_errors,
                number_of_errors,
                msg="errors: expected {} got {}\ncode {}".format(
                    expected_number_of_errors, number_of_errors, code)
            )
            self.assertEqual(
                expected_number_of_failures,
                number_of_failures,
                msg="failures: expected {} got {}\ncode {}".format(
                    expected_number_of_failures, number_of_failures, code)
            )
            for docstring in stat_dict['failure_docstrings']:
                self.assertIsInstance(docstring, str)
                self.assertIn('függvény', docstring)

    def test_get_statistics_and_messages_returns_with_proper_values(self):
        for code, expected_number_of_failures, expected_number_of_errors in codes:
            stat_dict = unittester.get_statistics_and_messages(
                code, unittest_code)
            number_of_errors = stat_dict["number_of_errors"
                                         ]
            number_of_failures = stat_dict["number_of_failures"]
            self.assertEqual(
                expected_number_of_errors,
                number_of_errors,
                msg="errors: expected {} got {}\ncode {}".format(
                    expected_number_of_errors, number_of_errors, code)
            )
            self.assertEqual(
                expected_number_of_failures,
                number_of_failures,
                msg="failures: expected {} got {}\ncode {}".format(
                    expected_number_of_failures, number_of_failures, code)
            )

    def test_testcase_statistics_and_messages_all_if_SyntaxError(self):
        codes_with_syntax_or_indentation_error = [
            """
def masodfoku()
    pass
""",
            """
 print(8)
"""]
        for code_with_error in codes_with_syntax_or_indentation_error:
            stat_dict = unittester.get_statistics_and_messages(
                code_with_error, unittest_code)
            self.assertIn(
                "row_number",
                stat_dict,
            )

    def test_save_code_and_unittest_save_the_proper_file(self):
        code = """print(8)"""
        unittest_code = """
import unittest """
        module_name = unittester.save_code_and_unittest(code, unittest_code)
        file_name = '/tmp/{}.py'.format(module_name)
        self.assertTrue(os.path.isfile(file_name))
        with open(file_name) as f:
            file_text = f.read()
        self.assertTrue(file_text.startswith('print(8)'))
        self.assertTrue("import unittest" in file_text)

    def test_we_can_import_module_after_save_code_and_unittest(self):
        code = """a = 8"""
        unittest_code = """
import unittest """
        module_name = unittester.save_code_and_unittest(code, unittest_code)
        file_name = '/tmp/{}.py'.format(module_name)
        self.assertTrue(os.path.isfile(file_name))
        exec('import {}'.format(module_name))
        a = eval('{}.a'.format(module_name))
        self.assertEqual(a, 8)

    def test_we_can_get_the_row_number_of_the_errors(self):
        for code in erroneous_codes:
            file_name = '/tmp/{}.py'.format(main.save_code_and_unittest(code))
            error_message = main.get_compile_error_message(file_name)
            if error_message == '':
                error_message = main.get_interpreter_error_message(file_name)
            self.assertEqual(main.get_row_number(error_message), 2)

if __name__ == '__main__':
    unittest.main()
