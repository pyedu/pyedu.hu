from math import sqrt


def diszkriminans(a, b, c):
    return b*b - 4*a*c


def masodfoku(*egyutthatok):
    # assert len(egyutthatok) == 3
    D = diszkriminans(*egyutthatok)
    a, b, c = egyutthatok
    x1 = (-b - sqrt(D)) / (2*a)
    x2 = (-b + sqrt(D)) / (1*a)  # Error!
    return x1, x2


import unittest


known_values = (
    ((1, 2, 1), (-1, -1), 0),
    ((1, -2, 1), (1, 1), 0),
    ((1, -1, -6), (3, -2), 25),
    ((1, -5, 6), (3, 2), 1),
)


class MasodfokuTest(unittest.TestCase):

    def test_masodfoku_returns_proper_values(self):
        "A masodfoku függvénynek helyes értékekkel kell visszatérnie."
        for args, return_values, _ in known_values:
            self.assertEqual(set(masodfoku(*args)), set(return_values))

    def test_masodfoku_raise_exception_if_number_of_args_not_3(self):
        "A masodfoku függvény kivételt dob, ha nem három argumentummal hívják."
        bad_args = ((1, 2, 1, 1), (1,), (1, 2))
        for args in bad_args:
            with self.assertRaises(AssertionError):
                masodfoku(*args)


class DiszkriminansTest(unittest.TestCase):

    def test_diszkriminans_returns_proper_values(self):
        "A diszkriminans függvénynek helyes értékekkel kell visszatérnie."
        for args, _, return_values in known_values:
            self.assertEqual(diszkriminans(*args), return_values)


test_cases = [MasodfokuTest, DiszkriminansTest]


def suite(*test_cases):
    suites = [unittest.makeSuite(case) for case in test_cases]
    return unittest.TestSuite(suites)


def testcase_statistics_and_messages(*test_cases):
    runner = unittest.TextTestRunner()
    test_suite = suite(*test_cases)
    test_result = runner.run(test_suite)

    failure_messages = [mesg for test_case, mesg in test_result.failures]
    number_of_failures = len(failure_messages)
    error_messages = [mesg for test_case, mesg in test_result.errors]
    number_of_errors = len(error_messages)
    number_of_test_cases = test_suite.countTestCases()
    number_of_successes = (number_of_test_cases
                           - number_of_errors - number_of_failures)

    return dict(
        number_of_test_cases=number_of_test_cases,
        failure_messages=failure_messages,
        error_messages=error_messages,
        number_of_successes=number_of_successes,
    )


if __name__ == '__main__':
    for key, value in testcase_statistics_and_messages(*test_cases).items():
        print("*" * 20)
        print(key, value)
