#!/usr/bin/env python3

import tempfile
import os
import unittest
import time
import sys
# import subprocess
import shlex
import re
from subprocess import Popen, PIPE


def save_code_and_unittest(code, unittest_code=''):
    _, file_name = tempfile.mkstemp(suffix=".py")
    module_name = os.path.split(file_name)[1][:-3]
    with open(file_name, "w", encoding='utf-8') as f:
        f.write(code)
        f.write(unittest_code)
    time.sleep(.01)  # without this modules are sometimes not imported
    # 0.01 seems to be enough 0.001 is too small.
    return module_name


# def has_syntax_error(file_name):
#     try:
#         subprocess.check_output(["/usr/bin/python3", "-m",
#                                  "py_compile", file_name])
#     except subprocess.CalledProcessError:
#         return True
#     return False


def syntax_error_dict(module_name):
    try:
        exec('import {}'.format(module_name))
    # except (ImportError, SyntaxError, IndentationError):
    except Exception:
        return syntax_or_others_error_dict(
            '/tmp/{}.py'.format(module_name))
    return {}


def syntax_or_others_error_dict(file_name):
    error_message = get_interpreter_error_message(file_name)
    if "Traceback" in error_message:
        return dict(error_message=error_message,)
    else:
        row_number = get_row_number(error_message)
        error_message = error_message.split("\n", 1)[1]
        return dict(row_number=row_number,
                    error_message=error_message,)


def get_compile_error_message(file):
    args = shlex.split("python3 -m py_compile {}".format(file))
    proc = Popen(args, stderr=PIPE)
    return proc.stderr.read().decode()


def get_interpreter_error_message(file):
    args = shlex.split("python3 {}".format(file))
    proc = Popen(args, stderr=PIPE)
    return proc.stderr.read().decode()


def get_row_number(error_message):
    first_line = error_message.splitlines()[0]
    pattern = re.compile(r"line (\d+)")
    result = pattern.search(first_line)
    if not result:
        first_line = error_message.splitlines()[1]
        result = pattern.search(first_line)
    return int(result.group(1))


def suite(*test_cases):
    suites = [unittest.makeSuite(case) for case in test_cases]
    return unittest.TestSuite(suites)


def testcase_statistics_and_messages(*test_cases):
    with open('/dev/null', "w") as devnull:
        runner = unittest.TextTestRunner(stream=devnull)
        test_suite = suite(*test_cases)
        test_result = runner.run(test_suite)

    failure_messages = [mesg for test_case, mesg in test_result.failures]
    failure_docstrings = [test_case._testMethodDoc for test_case, mesg
                          in test_result.failures]
    number_of_failures = len(failure_messages)
    error_messages = [mesg for test_case, mesg in test_result.errors]
    error_docstrings = [test_case._testMethodDoc for test_case, mesg
                        in test_result.errors]
    number_of_errors = len(error_messages)
    number_of_test_cases = test_suite.countTestCases()
    number_of_successes = (number_of_test_cases
                           - number_of_errors - number_of_failures)
    # print(number_of_failures, number_of_errors, number_of_test_cases)

    return dict(
        number_of_test_cases=number_of_test_cases,
        failure_messages=failure_messages,
        failure_docstrings=failure_docstrings,
        error_messages=error_messages,
        error_docstrings=error_docstrings,
        number_of_successes=number_of_successes,
        number_of_errors=number_of_errors,
        number_of_failures=number_of_failures,
    )


def get_statistics_and_messages(code, unittest):
    tmpdir = '/tmp'
    if tmpdir not in sys.path:
        sys.path.append(tmpdir)
    module_name = save_code_and_unittest(code, unittest)
    error_dict = syntax_error_dict(module_name)
    if error_dict:
        return error_dict
    exec('import {}'.format(module_name))
    test_case = eval('{}.test_cases'.format(module_name))
    stat_dict = testcase_statistics_and_messages(*test_case)
    return stat_dict
