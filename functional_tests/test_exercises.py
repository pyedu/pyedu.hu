#!/usr/bin/env python3
# coding: utf-8

"""functional tests for exercises
"""

import unittest
import time
from .test_general import MyStaticLiveServerTestCase, log_user_in


def sleep_a_bit():
    time.sleep(100)


class SingleChoiceTest(MyStaticLiveServerTestCase):
    """Test what a visitor not logged in can do."""

    def setUp(self):
        super().setUp()
        log_user_in(self, 'diak')

    def test_single_choice_score_page_has_radiobuttons(self):
        self.browser.get(self.server_url + '/course/1/2/21/ex/')
        submit = self.browser.find_element_by_xpath("//input[@id='answer2']")
        submit.click()
        submit = self.browser.find_element_by_xpath("//input[@type='submit']")
        submit.click()
        input_boxes = self.browser.find_elements_by_tag_name('input')
        self.assertEqual(len(input_boxes), 3)
        input1 = input_boxes[0]
        self.assertEqual(input1.get_attribute("type"), "radio")


if __name__ == "__main__":
    unittest.main(warnings="ignore")
