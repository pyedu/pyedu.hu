#!/usr/bin/env python3
# coding: utf-8

"""functional tests
"""

from selenium import webdriver
import unittest
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
import time
from courses import create_course
import sys
from selenium.common.exceptions import WebDriverException


def sleep_a_bit():
    time.sleep(100)


def log_user_in(self, username, password=None):
    if password is None:
        password = username
    self.browser.get(self.server_url + '/course/login/')
    input_boxes = self.browser.find_elements_by_tag_name('input')
    self.assertGreater(len(input_boxes), 2)
    _, username_input, password_input, _ = input_boxes
    username_input.send_keys(username)
    password_input.send_keys(password)
    submit = self.browser.find_element_by_css_selector(
        'form button[type="submit"]')
    submit.click()


def log_user_out(self):
    self.browser.get(self.server_url + '/course/logout/')


def normalize_url(server_url):
    if server_url.startswith('http://'):
        server_url = server_url[7:]
    if server_url[-1] == '/':
        server_url = server_url[:-1]
    return server_url


class MyStaticLiveServerTestCase(StaticLiveServerTestCase):

    @classmethod
    def setUpClass(cls):
        create_course.main()
        for arg in sys.argv:
            if 'liveserver' in arg:
                server_url = arg.split('=')[1]
                cls.server_url = 'http://' + normalize_url(server_url)
                cls.live_server_url = False
                return
        super().setUpClass()
        cls.server_url = cls.live_server_url

    @classmethod
    def tearDownClass(cls):
        if cls.server_url == cls.live_server_url:
            super().tearDownClass()

    def setUp(self):
        try:
            self.browser = webdriver.Firefox()
        except WebDriverException:
            self.browser = webdriver.Chrome()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        log_user_out(self)
        self.browser.quit()

    def assertAreNotInPage(self, text_list):
        for text in text_list:
            self.assertNotIn(text, self.browser.page_source)

    def assertAreInPage(self, text_list):
        for text in text_list:
            self.assertIn(text, self.browser.page_source)


class NewVisitorTest(MyStaticLiveServerTestCase):
    """Test what a visitor not logged in can do."""

    def test_course_page_has_the_proper_content(self):
        log_user_in(self, 'diak')
        self.browser.get(self.server_url + '/course/')
        self.assertIn('pyEdu', self.browser.title)
        self.assertAreInPage([
            'Az összetett',
            'Tesztkurzus formális nyelvekről',
            'Tesztkurzus hálózatokról']
        )

        course_link = self.browser.find_element_by_partial_link_text(
            "Tesztkurzus háló")
        course_link.click()
        self.assertEqual(self.server_url + "/course/2/",
                         self.browser.current_url)
        self.assertAreInPage(["Tesztkurzus", "A csoporterő", "Bevezetés"])

    def test_student_can_login(self):
        self.browser.get(self.server_url + '/course/login/')
        input_boxes = self.browser.find_elements_by_tag_name('input')
        self.assertGreater(len(input_boxes), 2)
        csrf, username_input, password_input, hidden_input = input_boxes
        username_input.send_keys('diak')
        password_input.send_keys('diak')
        submit = self.browser.find_element_by_css_selector(
            'form button[type="submit"]')
        submit.click()
        self.assertEqual(self.server_url + "/course/",
                         self.browser.current_url)

    def test_student_login_go_back_to_original_page(self):
        self.browser.get(self.server_url + '/course/2/2/')
        input_boxes = self.browser.find_elements_by_tag_name('input')
        self.assertGreater(len(input_boxes), 2)
        csrf, username_input, password_input, hidden_input = input_boxes
        username_input.send_keys('diak')
        password_input.send_keys('diak')
        submit = self.browser.find_element_by_css_selector(
            'form button[type="submit"]')
        submit.click()
        self.assertEqual(self.server_url + "/course/2/2/",
                         self.browser.current_url)

    def test_admin_can_login_to_admin_page(self):
        log_user_in(self, 'admin')
        self.browser.get(self.server_url + '/admin/')
        video_model_link = self.browser.find_element_by_link_text("Videos")
        video_model_link.click()
        self.assertEqual(self.server_url + "/admin/videos/",
                         self.browser.current_url)

    def test_video_pages_are_available(self):
        self.browser.get(self.server_url + '/course/1/1/2/qs/')

        input_boxes = self.browser.find_elements_by_tag_name('input')
        self.assertGreater(len(input_boxes), 2)
        csrf, username_input, password_input, hidden_input = input_boxes
        username_input.send_keys('diak')
        password_input.send_keys('diak')
        submit = self.browser.find_element_by_css_selector(
            'form button[type="submit"]')
        submit.click()
        for url_end in (
            '2/2/1/nk/',
            '2/2/1/so/',
            '2/2/1/qs/',
        ):
            self.browser.get(self.server_url + '/course/' + url_end)
            video = self.browser.find_element_by_tag_name("video")
            self.assertAlmostEqual(
                video.size['width'],
                450,
                delta=3
            )

    def test_layout_and_styling(self):
        self.browser.get(self.server_url + "/course/")
        width, height = 1024, 768
        self.browser.set_window_size(width, height)

        main_div = self.browser.find_element_by_class_name('jumbotron')
        self.assertAlmostEqual(
            main_div.location['x'] + main_div.size['width']/2,
            width/2,
            delta=2
        )


class StudentTest(MyStaticLiveServerTestCase):
    """Test logged in student"""

    def setUp(self):
        super().setUp()
        log_user_in(self, 'diak')

    def test_student_can_do_simple_choice(self):
        self.browser.get(self.server_url + '/course/1/2/21/ex/')
        submit = self.browser.find_element_by_xpath("//input[@id='answer2']")
        submit.click()
        submit = self.browser.find_element_by_xpath("//input[@type='submit']")
        submit.click()
        self.assertAreInPage(["pontszám", "nem fordulhat", "Kurzusok"])

    def test_student_can_not_do_simple_choice_without_answer(self):
        url = self.server_url + '/course/1/2/21/ex/'
        self.browser.get(url)
        submit = self.browser.find_element_by_xpath("//input[@type='submit']")
        submit.click()
        self.assertEqual(self.browser.current_url, url)
        submit_buttons = self.browser.find_elements_by_xpath(
            "//input[@type='submit']")
        self.assertEqual(len(submit_buttons), 1)
        self.assertAreInPage(["Válasszon ki"])
        self.assertAreNotInPage(["pontszám"])

    def test_student_can_see_the_notes(self):
        note1 = "This is a note."
        note2 = "This is another note."
        url_of_item_with_note = (
            ('/course/1/2/21/nk/', note1),
            ('/course/1/2/21/ex/', note1),  # choice
            ('/course/1/5/2/ex/', note2),  # pycode
        )
        for url, note in url_of_item_with_note:
            self.browser.get(self.server_url + url)
            self.assertAreInPage([note, "Megjegyzések"])

    def test_student_can_not_see_notes_where_is_not(self):
        url_of_item_without_note = (
            '/course/1/2/10/nk/',
            '/course/1/1/2/qs/',
            '/course/1/1/2/ex/',
        )
        for url in url_of_item_without_note:
            self.browser.get(self.server_url + url)
            self.assertAreNotInPage(["Megjegyzések"])


class AnonymousTest(MyStaticLiveServerTestCase):
    """Test logged in student"""

    def setUp(self):
        super().setUp()
        log_user_in(self, 'anonymous', 'anonymous')

    def test_0_anonymous_can_log_in(self):
        self.assertEqual(self.server_url + "/course/",
                         self.browser.current_url)

    def test_1_anonymous_can_submit_simple_choice_without_answer(self):
        self.browser.get(self.server_url + '/course/1/2/21/ex/')
        submit = self.browser.find_element_by_xpath("//input[@type='submit']")
        submit.click()
        self.assertAreInPage(["Válasszon ki"])
        self.assertAreNotInPage(["pontszám"])

    def test_2_anonymous_can_complete_simple_choice_without_answer(self):
        self.browser.get(self.server_url + '/course/1/2/21/ex/')
        answer2 = self.browser.find_element_by_xpath("//input[@id='answer2']")
        answer2.click()
        submit = self.browser.find_element_by_xpath("//input[@type='submit']")
        submit.click()
        self.assertAreInPage(["pontszám", "nem fordulhat", "5 / 5"])

    def test_3_anonymous_can_not_see_its_scores(self):
        self.browser.get(self.server_url + '/course/1/2/21/ex/')
        answer2 = self.browser.find_element_by_xpath("//input[@id='answer2']")
        answer2.click()
        submit = self.browser.find_element_by_xpath("//input[@type='submit']")
        submit.click()
        self.assertAreInPage(["pontszám", "nem fordulhat", "5 / 5"])
        self.assertAreNotInPage(["Pontjaim"])

    def test_4_anonymous_choice_doesnt_stored(self):
        pass

if __name__ == "__main__":
    unittest.main(warnings="ignore")
