#!/usr/bin/env python3


import tempfile
import os
import unittest
from exercises.models import Code


def save_code_and_unittest(code, unittest_code):
    #sys.path.append('/tmp')
    _, file_name = tempfile.mkstemp(suffix=".py")
    module_name = os.path.split(file_name)[1][:-3]
    print(module_name)
   # print(file_name)
    with open(file_name, "w") as f:
         #f.write("#!/usr/bin/env python3")
         f.write(code)
         f.write(unittest_code)
    #data save database
    #dcode = Code.objects.create(test=unittest_code, test_type='1')
    return module_name


def suite(*test_cases):
    suites = [unittest.makeSuite(case) for case in test_cases]
    return unittest.TestSuite(suites)


def testcase_statistics_and_messages(*test_cases):
    runner = unittest.TextTestRunner()
    test_suite = suite(*test_cases)
    test_result = runner.run(test_suite)

    failure_messages = [mesg for test_case, mesg in test_result.failures]
    number_of_failures = len(failure_messages)
    error_messages = [mesg for test_case, mesg in test_result.errors]
    number_of_errors = len(error_messages)
    number_of_test_cases = test_suite.countTestCases()
    number_of_successes = (number_of_test_cases
                           - number_of_errors - number_of_failures)

    return dict(
        number_of_test_cases=number_of_test_cases,
        failure_messages=failure_messages,
        error_messages=error_messages,
        number_of_successes=number_of_successes,
        number_of_errors=number_of_errors,
        number_of_failures=number_of_failures,

    )
