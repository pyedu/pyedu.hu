from django.shortcuts import render
import unittester
import sys
from exercises.score import save_score_and_answer
import courses
from exercises.answers import nice_approximated_number_format


def index(request):

    return render(request, 'pycodes/code.html')


def calculate_score_factor(stat_dict):
    if not stat_dict:
        return 0
    return stat_dict["number_of_successes"] / stat_dict["number_of_test_cases"]


def code_exercise(request, exercise):
    sys.path.append('/tmp')
    unit = exercise.unit
    next_item = courses.models.next_item(exercise)
    if request.method == 'POST':
        unittest = exercise.code.test

        submitted_code = request.POST['code']
        stat_dict = unittester.get_statistics_and_messages(
            submitted_code, unittest)
        if "error_message" in stat_dict:
            score_value = 0
        else:
            score_value = unit.max_score * calculate_score_factor(stat_dict)
        score = save_score_and_answer(
            request.user.username,
            unit,
            score_value=score_value,
            answer=submitted_code)
        context = {
            'max_score': unit.max_score,
            'exercise': exercise,
            'answer': submitted_code,
            'score': nice_approximated_number_format(score.score),
            'unit': unit,
            'next_item': next_item,
        }

        context.update(stat_dict)
        return render(request, 'pycodes/code.html', context)

    scores = unit.score_set.filter(user__username=request.user.username)
    if scores:
        answer = scores[0].answer
    else:
        answer = ''
    return render(
        request, 'pycodes/code.html',
        {'exercise': exercise, 'unit': unit, 'next_item': next_item,
         'answer': answer}
    )
