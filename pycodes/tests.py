from django.test import TestCase
from pycodes import views


class ScoreCalculationTest(TestCase):
    def test_calculate_score_returns_proper_values(self):
        known_values = [
            (
                {"number_of_successes": 1,
                 "number_of_test_cases": 3},
                1/3
            ),
            (
                {"number_of_successes": 2,
                 "number_of_test_cases": 3},
                2/3
            ),
            (
                {"number_of_successes": 3,
                 "number_of_test_cases": 3},
                1
            ),
        ]

        for stat_dict, factor in known_values:
            self.assertAlmostEqual(
                views.calculate_score_factor(stat_dict),
                factor)
