from django.conf.urls import include, url
from courses import views
import discussions

from django.contrib import admin
admin.autodiscover()

urlpatterns = [
    # Examples:
    url(r'^$', views.index, name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/logout/', views.user_logout, name='adminlogout'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^course/', include('courses.urls')),
    url(r'^discussion/', include('discussions.urls')),
    url(r'^create_discussion/(\d+)/(\d+)/(\d+)/$',
        discussions.views.create_discussion,
        name='create_discussion'
        ),
    # url(r'^login/', 'django.contrib.auth.views.login'),
]
