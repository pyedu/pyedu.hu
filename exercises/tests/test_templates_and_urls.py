from django.core.urlresolvers import resolve
from django.template.loader import render_to_string
from django.http import HttpRequest
from videos.views import item
from courses.models import User
from courses.tests.base import CourseTestCase


class ExercisePageTest(CourseTestCase):
    def test_exercise_url_resolves_the_item_page_view(self):
        for type_ in ("ex", "nk", "qs", "so"):
            found = resolve('/course/1/1/1/{}/'.format(type_))
            self.assertEqual(found.func, item)

    def test_item_page_returns_correct_html(self):
        request = HttpRequest()
        request.user = User(username="student",
                            password="student",
                            email="joe@students.uni-pyedu.edu")
        response = item(request, '1', '1', '2', 'ex')
        unit = self.get_units(1)[2]
        exercise = unit.get_exercise()
        choice = exercise.choice
        context = {
            'choice': choice,
            'answer_list': choice.answer_set.all(),
            'next_item': unit.next_item(exercise),
            'unit': unit,
        }
        expected_html = render_to_string(
            'exercises/exercise_details.html',
            context=context,
            request=request
        )
        self.assertEqualExceptCSFR(
            response.content.decode(),
            expected_html
        )
