from django.test import TestCase
from exercises import views
from exercises import answers
from exercises import gift
from courses import models
from courses.create_course import main


class SaveScoreTest(TestCase):
    def setUp(self):
        main()
        self.unit = models.Unit.objects.get(
            lesson__title='Reguláris kifejezések',
            serial=2)

    def test_save_score_and_answer_saves_the_data(self):
        unit = self.unit
        answer = "12,123,265"
        answer2 = "12,123"
        score1 = views.save_score_and_answer(
            username="diak", unit=unit, score_value=3, answer=answer)
        self.assertEqual(unit.score_set.count(), 1)
        self.assertEqual(score1.score, 3)
        self.assertEqual(score1.answer, answer)
        score2 = views.save_score_and_answer(
            username="diak", unit=unit, score_value=5, answer=answer2)
        self.assertEqual(unit.score_set.count(), 1)
        self.assertEqual(score2.score, 5)
        self.assertEqual(score2.answer, answer2)

    def test_save_score_and_answer_does_not_create_duplicate(self):
        unit = self.unit
        answer = "12,123,265"
        views.save_score_and_answer(
            username="diak", unit=unit, score_value=3, answer=answer)
        views.save_score_and_answer(
            username="diak", unit=unit, score_value=4, answer=answer)
        self.assertEqual(unit.score_set.count(), 1)

    def test_save_score_and_answer_raises_exeption_maxscore(self):
        unit = self.unit
        answer = "12,123,265"
        with self.assertRaises(AssertionError):
            views.save_score_and_answer(
                username="diak", unit=unit, score_value=6, answer=answer)


class ExerciseModelTest(TestCase):
    def setUp(self):
        main()

    def test_get_subtype_get_proper_type(self):
        course = models.Course.objects.get(
            title="Tesztkurzus formális nyelvekről")
        known_values = [
            (course.get_element(1, 2, 'ex'), 'choice'),
            (course.get_element(2, 21, 'ex'), 'choice'),
            (course.get_element(5, 2, 'ex'), 'code'),
        ]
        for exercise, subtype in known_values:
            self.assertEqual(exercise.get_subtype(), subtype)


class JoinAnswerTest(TestCase):
    def test_join_answer_list(self):
        known_values = [
            (
                {
                    "answer121": "on",
                    "answer12": "on",
                    "answer130": "off",
                    "cssfr": "on",
                },
                "12,121"
            ),
            (
                {
                    "answer121": "off",
                    "answer12": "on",
                    "answer130": "on",
                    "cssfr": "on",
                },
                "12,130"
            ),
            (
                {
                    "answer121": "on",
                    "answer12": "on",
                    "answer130": "on",
                    "come": "on",
                },
                "12,121,130"
            ),
            (
                {
                    "answer21": "on",
                    "answer9": "on",
                    "answer130": "on",
                    "come": "on",
                },
                "9,21,130"
            ),
        ]
        for dict_, return_value in known_values:
            self.assertEqual(answers.join_multiple_answer_values(dict_),
                             return_value)


class Answer:
    def __init__(self, id, text, score):
        self.id = id
        self.text = text
        self.score = score

    def __str__(self):
        return '{0.id} {0.text} {0.score}'.format(self)

    def __repr__(self):
        return str(self)


def create_answers(answer_dicts):
    return [Answer(id, text, score)
            for id, (text, score) in enumerate(answer_dicts, 1)
            ]


class SumAnswerScore(TestCase):
    answer_dicts = (
        ("*", 0.2),
        (".", 0.8),
        ("&", -0.5),
        ("@", -0.5),
    )
    dicts_ = (
        (
            {
                "answer1": "on",
                "answer2": "off",
                "answer3": "off",
                "answer4": "off",
                "cssfr": "on",
            },
            .2
        ),
        (
            {
                "answer1": "off",
                "answer2": "on",
                "answer3": "off",
                "answer4": "off",
                "cssfr": "on",
            },
            .8
        ),
        (
            {
                "answer1": "on",
                "answer2": "on",
                "answer3": "on",
                "answer4": "off",
                "cssfr": "on",
            },
            .5
        ),
        (
            {
                "answer1": "on",
                "answer2": "on",
                "answer3": "off",
                "answer4": "off",
                "cssfr": "on",
            },
            1
        ),
        (
            {
                "answer1": "on",
                "answer2": "on",
                "answer3": "on",
                "answer4": "on",
                "cssfr": "on",
            },
            0
        ),
        (
            {
                "answer1": "off",
                "answer2": "off",
                "answer3": "on",
                "answer4": "on",
                "cssfr": "on",
            },
            0
        ),
    )

    def test_sum_answer_score(self):
        answers_ = create_answers(self.answer_dicts)
        for dict_, score in self.dicts_:
            return_value = answers.sum_answer_score(dict_, answers_)
            self.assertEqual(score, return_value)


class NiceNumber(TestCase):
    known_values = [
        (1/6, 0.17),
        (1/2, 0.5),
        (1/9, 0.11),
        (5/9, 0.56),
        (1.0, 1),
        (1/3, 0.33),
        (2/3, 0.67),
        (5.0, 5),
        (4.9999995, 5),
        (1.299993, 1.3),
        (0.12995, 0.13)]

    def test_nice_approximated_number_format(self):
        for arg, result in self.known_values:
            self.assertEqual(answers.nice_approximated_number_format(arg),
                             result)


class Gift(TestCase):
    known_values = [
        ([0, 1, 0, 0, 0], False),
        ([0., 1., 0., 0., 0.], False),
        ([0, .9, .1, 0, 0], True),
        ([0., .1, 0., .9, 0.], True),
        ([0, 1, 1, 0, 0], True),
        ([0., 1., 0., 1., 0.], True),
        ]

    def test_is_multiple(self):
        for answer_scores, value in self.known_values:
            self.assertEqual(gift.is_multiple(answer_scores), value)
