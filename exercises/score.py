from courses import models


def save_score_and_answer(username, unit, score_value, answer):
    assert 0 <= score_value <= unit.max_score
    scores = unit.score_set.filter(user__username=username)
    if scores:
        score = scores[0]
        score.score = score_value
        score.answer = answer
    else:
        user = models.User.objects.get(username=username)
        score = models.Score(unit=unit, user=user,
                             score=score_value, answer=answer)
    score.save()
    return score
