from fractions import Fraction
__author__ = 'Árpád Horváth'

TRUE, FALSE, NOT_SELECTED = 'true', 'false', 'not_selected'


def get_id_list(dict_, prefix="answer"):
    if 'answer' in dict_:
        return [str(dict_["answer"])]
    return [
        key[len(prefix):] for key, value in dict_.items()
        if key.startswith(prefix) and value == 'on'
        # TODO perhaps we do not need the previous line
    ]


def join_multiple_answer_values(dict_):
    id_list = sorted(get_id_list(dict_), key=int)
    return ",".join(id_list)


def sum_answer_score(dict_, answer_list):
    score = 0
    for ans in answer_list:
        for ids in get_id_list(dict_):
            if str(ans.id) == ids:
                score = score + ans.score
    return max(score, 0)


def answer_status_list(id_list, answer_list):
    a_list = []
    for answer in answer_list:
        if str(answer.id) in id_list:
            if answer.score > 0:
                status = TRUE
            if answer.score <= 0:
                status = FALSE
        else:
            status = NOT_SELECTED
        a_list.append((answer.text, status))
    return a_list


def nice_approximated_number_format(num):
    fraction = Fraction(num).limit_denominator(100)
    if fraction.denominator is 1:
        return fraction.numerator
    else:
        return round(float(fraction), 2)
