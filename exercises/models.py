from django.db import models
from courses.models import Unit
from django.core.exceptions import ObjectDoesNotExist


exercise_types = ["choice", "code"]


class Exercise(models.Model):
    unit = models.OneToOneField(Unit)
    text = models.CharField(max_length=300)
    type = 'ex'

    def get_subtype(self):
        for subtype in exercise_types:
            try:
                eval("self.{}".format(subtype))
                return subtype
            except ObjectDoesNotExist:
                pass


class Choice(Exercise):
    is_multiple = models.BooleanField()
    image_file_name = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return "Choice of unit {}".format(self.unit.title or self.unit.serial)


class Answer(models.Model):
    choice = models.ForeignKey(Choice)
    text = models.CharField(max_length=300)
    score = models.FloatField()

    class Meta:
        ordering = ['pk']


class Code(Exercise):
    initial_code = models.TextField(default="")
    test = models.TextField()
    test_type = models.IntegerField(
        choices=((1, "unittest"), (2, "assert")),
        default=1
    )
