from django.shortcuts import render
from pycodes import views as pycodes_views
import courses
from .answers import (
    join_multiple_answer_values, sum_answer_score,
    answer_status_list, nice_approximated_number_format, get_id_list
)
from .score import save_score_and_answer


def exercise(request, unit):
    view = {
        'choice': choice,
        'code': pycodes_views.code_exercise,
    }[unit.exercise.get_subtype()]
    return view(request, unit.exercise)


def single_choice_without_answer(request, exercise, context):
    context.update({
        'error_message': 'Válasszon ki legalább egy válaszlehetőséget!',
    })
    return render(request, 'exercises/exercise_details.html', context)


def choice(request, exercise):
    choice = exercise.choice
    unit = exercise.unit
    next_item = courses.models.next_item(exercise)
    answer_list = choice.answer_set.all()
    context = {
        'choice': choice,
        'answer_list': answer_list,
        'next_item': next_item,
        'unit': unit,
    }
    if request.method == 'POST':
        if choice.is_multiple:
            answer = join_multiple_answer_values(request.POST)
            score =  unit.max_score * sum_answer_score(request.POST, answer_list)
        else:
            if 'answer' in request.POST:
                answer = request.POST['answer']
                score = unit.max_score * choice.answer_set.get(id=int(answer)).score
            else:
                return single_choice_without_answer(request, exercise, context)
        save_score_and_answer(request.user.username, unit, score, answer)
        id_list = get_id_list(request.POST)
        context = {
            'answer_status_list': answer_status_list(id_list, answer_list),
            'score': nice_approximated_number_format(score),
            'answer': answer,
            'id_list': id_list,
            'unit': unit,
            'next_item': next_item,
            'input_type': 'checkbox' if choice.is_multiple else 'radio',
        }
        return render(request, 'exercises/score.html', context)
    return render(request, 'exercises/exercise_details.html', context)
