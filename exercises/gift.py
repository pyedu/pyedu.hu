from pygiftparser import parser


def create_question_from_gift(gift):
    return parser.Question(gift, "", "")


def is_multiple(answer_scores):
    sorted_scores = sorted(answer_scores)
    if sorted_scores.pop() != 1:
        return True
    return any(i != 0 for i in sorted_scores)


def create_choice_dict_from_gift(gift):
    question = create_question_from_gift(gift)
    answers = [(answer.answer, answer.fraction/100)
               for answer in question.answers.answers]
    choice_dict = dict(
        type="choice",
        is_multiple=is_multiple([score for _, score in answers]),
        text=question.text,
        answers=tuple(answers),
    )
    return choice_dict
