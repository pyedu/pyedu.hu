from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from discussions.models import DiscussionForm, Discussion, AnswerForm
from django.http import HttpResponseRedirect
# from django.form import formset_factory
from courses.models import Unit


@login_required
def create_discussion(request, unit_id):
    unit = Unit.objects.get(id=unit_id)
    if request.method == 'POST':
        form = DiscussionForm(request.POST)
        if form.is_valid():
            discussion = form.save(commit=False)
            discussion.unit = unit
            discussion.user = request.user.user
            discussion.save()
            return HttpResponseRedirect(unit.get_absolute_url())
    else:
        form = DiscussionForm()
    return render(
        request,
        'discussions/create_discussion.html',
        {'form': form, 'unit': unit}
    )


@login_required
def discussion(request, discussion_id):
    discussion = Discussion.objects.get(id=discussion_id)
    if request.method == 'POST':
        form = AnswerForm(request.POST)
        if form.is_valid():
            answer = form.save(commit=False)
            answer.discussion = discussion
            answer.user = request.user.user
            answer.save()
            form = AnswerForm()
    else:
        form = AnswerForm()
    return render(
        request,
        'discussions/discussion.html',
        {'form': form, 'discussion': discussion}
    )
