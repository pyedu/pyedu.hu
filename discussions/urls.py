from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^(\d+)/$',
        views.discussion,
        name='discussion_details'
        ),
    url(r'^create/(\d+)/$',
        views.create_discussion,
        name='create_discussion'
        ),
]
