from django.db import models
from django.forms import ModelForm
from courses.models import User, Unit
from django.core.urlresolvers import reverse


class Discussion(models.Model):
    user = models.ForeignKey(User)
    unit = models.ForeignKey(Unit)
    summary = models.CharField(
        'Összefoglaló',
        max_length=100,
        help_text='A kérdés tömör, egy soros összefoglalója, '
        'ami a felsorolások esetén látszik.')
    question = models.TextField(
        'Kérdés',
        max_length=500,
        help_text='Itt részletezzen mindent, '
        'ami a kérdés szempontjából fontos! '
        'A kérdés az összefoglaló nélkül, '
        'önmagában is értelmes legyen.'
    )
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-timestamp']

#    def get_absoulte_url(self):
#       return reverse(
#            'unit',
#            args=(str(i) for i in
#                  (self.lesson.course.id,
#                   self.lesson.serial,
#                   self.serial)
#                  )
#        )

    def get_absolute_url(self):
        return reverse('discussion_details', args=[str(self.id)])


class Answer(models.Model):
    discussion = models.ForeignKey(Discussion)
    user = models.ForeignKey(User)
    text = models.TextField(
        'Válasz szövege',
        max_length=500,
    )
    timestamp = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-timestamp']


class DiscussionForm(ModelForm):
    class Meta:
        model = Discussion
        fields = ['summary', 'question']


class AnswerForm(ModelForm):
    class Meta:
        model = Answer
        fields = ['text']
