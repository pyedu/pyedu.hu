from courses.tests.tests_for_models import ModelTestCase
import discussions


class DiscussionTestCase(ModelTestCase):
    def setUp(self):
        super().setUp()
        self.unit1 = self.course.get_element(1, 2)
        self.discussion1 = self.unit1.discussion_set.create(
            user=self.student1,
            unit=self.unit1,
            summary='Question of student1',
            question='What are the significance of regular expressions?',
        )
        self.unit2 = self.course.get_element(2, 1)
        self.discussion2 = self.unit2.discussion_set.create(
            user=self.student2,
            unit=self.unit2,
            summary='Question of student2',
            question='There can be final automaton with 0 final states?',
        )


class DiscussionTests(DiscussionTestCase):
    def test_saving_and_retreiving_discussions(self):
        d1 = discussions.models.Discussion.objects.get(
            summary__endswith='student1')
        self.assertTrue(d1.question.startswith('What are'))
        d2 = discussions.models.Discussion.objects.get(
            summary__endswith='student2')
        self.assertTrue(d2.question.startswith('There can'))


class AnswerTests(DiscussionTestCase):
    def test_saving_and_retreiving_answers(self):
        discussion1_answers = (
            'We can match URLs in django with them for example.',
            'We can check validity with a short and clear code.'
        )
        for answer in discussion1_answers:
            self.discussion1.answer_set.create(
                user=self.student2,
                text=answer,
            )
        discussions.models.Answer.objects.create(
            discussion=self.discussion2,
            user=self.student1,
            text='Yes, but it accepts the empty language, '
            'i.e. there is no word in it.'
        )
        self.assertEqual(self.discussion1.answer_set.count(), 2)
        self.assertEqual(self.discussion2.answer_set.count(), 1)
        answers = self.discussion1.answer_set.all()
        self.assertEqual(set(a.text for a in answers), set(discussion1_answers))
