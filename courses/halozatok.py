# It can be modified, but it was included in both courses in this modul
cl_unittest = """
import unittest


def my_clustering(edges, degree):
    assert edges >= 0
    assert degree >= 0
    assert isinstance(edges, int) and isinstance(degree, int)
    if degree < 2:
        return None
    max_edges = degree*(degree - 1) / 2
    return edges / max_edges


class Clustering(unittest.TestCase):

    def test_clustering_has_object(self):
        "A clustering objektum nem lett létrehozva!"
        self.assertIn("clustering", globals())

    def test_clustering_has_function(self):
        "A clustering objektum nem függvényként van definiálva!"
        self.assertTrue(hasattr(clustering, '__call__'))

    def test_clustering_raise_exception_if_edge_negative(self):
        "A függvény hibát dob, ha  az edges-nek negatív számot adtak meg!"
        bad_args = ((-1, 2), (-2, 3), (-0.1, 1))
        for args in bad_args:
            with self.assertRaises(AssertionError):
                clustering(*args)

    def test_clustering_raise_exception_if_both_args_are_negative(self):
        "A függvény hibát dob, ha  mindkét argumentum negatív!"
        bad_args = ((-1, -2), (-2, -3), (-1, -1))
        for args in bad_args:
            with self.assertRaises(AssertionError):
                clustering(*args)

    def test_clustering_raise_exception_if_degree_negative(self):
        "A függvény hibát dob, ha a fokszám negatív!"
        bad_args = ((0, -0.5), (0, -1))
        for args in bad_args:
            with self.assertRaises(AssertionError):
                clustering(*args)

    def test_clustering_raise_exception_if_degree_and_edge_not_integer(self):
        "A függvény hibát dob, ha a két argumentum nem egész típusú!"
        bad_args = ((0.5, 0.2), (1.2, 2.5))
        for args in bad_args:
            with self.assertRaises(AssertionError):
                clustering(*args)

    def test_clustering_return_None_for_degrees_lt_2(self):
        "A függvénynek None-nal kell visszatérnie, ha a fokszám < 2!"
        for degree in [0, 1]:
            self.assertEqual(None, clustering(0, degree))

    def test_clustering_return_proper_numeric_value(self):
        "A függvénynek helyes számértékkel kell visszatérnie!"
        for degree in range(2, 20):
            for edges in range(0, degree*(degree-1)//2+1):
                self.assertEqual(my_clustering(edges, degree),
                                 clustering(edges, degree))

test_cases = [Clustering]
"""

test_video_name = "network-basenetwork6"


def remove_video_extension(video_name):
    for ext in ".ogv .webm .mp4 .avi".split():
        if video_name.lower().endswith(ext):
            return video_name[:-len(ext)]
    return video_name


def create_pairs_from_string(string):
    pair_list = []
    for line in string.splitlines():
        if line:
            video, title = line.strip().split(",")
            video = remove_video_extension(video)
            pair_list.append((video, title))
    return pair_list


def create_units_from_pairs(pair_list):
    return [
        dict(
            serial=serial,
            title=title,
            items=[
                dict(
                    type="nk",
                    file_name=video,
                ),
            ]
        )
        for serial, (video, title) in enumerate(pair_list, 1)
    ]


def create_units_from_string(string):
    return create_units_from_pairs(create_pairs_from_string(string))


def get_max_unit_serial(unit_list):
    serials = [unit["serial"] for unit in unit_list]
    return max(serials)

# Összetett hálózatok (Hungarian course)

alapok = dict(
    serial=1,
    min_score=50,
    title="Bevezetés és alapfogalmak",
    units=create_units_from_string("""
1modul/scc_egy_komponens_meghatarozasa,Egy erősen összefüggő komponens meghatározása
1modul/scc_komponensekre_bontas,Komponensekre bontás
1modul/scc_komponenshatarnal_megallas,Miért állhatunk meg komponenshatárnál
"""),
)

python_bevezeto = dict(
    serial=2,
    min_score=50,
    title="Bevezetés a Python nyelvbe",
    units=[
        dict(
            serial=1,
            max_score=5,
            title="Szótár létrehozása",
            items=[
                dict(
                    type="nk",
                    file_name=test_video_name,
                ),
                dict(
                    type="qs",
                    file_name=test_video_name,
                ),
                dict(
                    type="choice",
                    is_multiple=True,
                    text="Melyik módon hozhatunk létre "
                    "szótárat Pythonban?",
                    answers=(
                        ("{'a': 4}", 0.5),
                        ("{'a', 'b', 'c'}", -0.5),
                        ("dict(a=4)", 0.5),
                        ("{a:4}", -0.5),
                    ),
                ),
                dict(
                    type="so",
                    file_name=test_video_name,
                    url="http://localhost/media/",
                ),
            ],
        ),
    ]
)

clustering_coeff = dict(
    serial=63,
    min_score=50,
    title="Csoporterősségi együttható",
    units=["""
serial: 1
title: Bevezető példa
url: django
nk: clustering_coeff/clustering_coeff-01
ex: Mekkora lesz Z csoporterősségi együtthatója?
           { ~ 1/10
           ~ 1/4
           ~ 5/6
           = 1/2
           }
so: clustering_coeff/clustering_coeff-02
""", """
serial: 3
title:  Teljes hálózat éleinek száma?
url: django
qs: clustering_coeff/clustering_coeff-03
ex: Mennyi él lehet maximálisan a ki szomszéd között? (Nem lehet többszörös él és hurokél.)
           { ~ ki*ki/3
           = ki*(ki-1)/2
           ~ ki*(ki+1)/2
           ~ ki*(ki-2)/4
           }
so: clustering_coeff/clustering_coeff-04
""", """
serial: 5
title: Csúcs csoporterősségi együtthatójának kiszámítása
url: django
qs: clustering_coeff/clustering_coeff-05
ex: Hogyan számítható egy csúcs csoporterősségi együtthatója? (Nem lehet többszörös él és hurokél.)
           { ~ Ei/3*ki
           ~ Ei / (2*ki*(ki-1))
           = 2*Ei / (ki*(ki-1))
           ~ Ei*(ki-2)/4
           }
so: clustering_coeff/clustering_coeff-06
""",
           dict(
               serial=6,
               max_score=5,
               title="A csoporterősségi együtthatót kiszámító függvény",
               items=[
                   dict(
                       type="qs",
                       url="django",
                       file_name="clustering_coeff/clustering_prog_qs",
                   ),
                   dict(
                       type="code",
                       text="""Írjon egy függvényt clustering néven (edges, degree) argumentumokkal, amely
                       csoporterősségi együtthatóval tér vissza, 0 és 1-es fokszám esetén None-nal!
                       Ha nem egészek az argumentumok, vagy
                       túl kicsi az értékük, adjon AssertionError-t a függvény!
                       """,
                       test=cl_unittest,
                   ),
                   dict(
                       type="so",
                       url="django",
                       file_name="clustering_coeff/clustering_prog_so",
                   ),
               ],
           ),
           """
serial: 7
title: Hálózat csoporterősségi együtthatójának kiszámítása
url: django
nk: clustering_coeff/clustering_coeff-07
""", """
serial: 8
title: 1. csúcs
url: django
qs: clustering_coeff/clustering_coeff-08
ex: Mekkora lesz Z csoporterősségi együtthatója?
           { ~ 1/10
           ~ 1/4
           = 0
           ~ 2/3
           }
so: clustering_coeff/clustering_coeff-09
""", """
serial: 10
title: 3. csúcs
url: django
qs: clustering_coeff/clustering_coeff-10
ex: Mekkora lesz a hármas csúcs csoporterősségi együtthatója?
           { ~ nincs értelmezve
           ~ 1/4
           ~ 5/6
           = 1/3
           }
so: clustering_coeff/clustering_coeff-11
""", """
serial: 12
title: C=?
url: django
qs: clustering_coeff/clustering_coeff-12
ex: Mekkora lesz a hálózat csoporterősségi együtthatója?
           { ~ 0
           ~ 13/7
           = 23/70
           ~ 34/80
           }
so: clustering_coeff/clustering_coeff-13
""", """
serial: 14
title: Négyzetrács-hálózat
url: django
qs: clustering_coeff/clustering_coeff-14
ex: Mekkora lesz a hálózat csoporterősségi együtthatója?
           { = 0
           ~ 13/36
           ~ 1
           ~ 34/80
           }
so: clustering_coeff/clustering_coeff-15
""", """
serial: 16
title: Piramis-példa
url: django
qs: clustering_coeff/clustering_coeff-16
ex: Milyen csoporterősségi együtthatók lesznek a hálózatban?
           { ~%-33.3% 0
           ~%-33.3% 13/36
           ~%33.3% 1
           ~%-33.3% 36/5
           ~%33.3% 1/2
           ~%33.3% 2/5
           }
so: clustering_coeff/clustering_coeff-17
""", """
serial: 18
title: C=?
url: django
nk: clustering_coeff/clustering_coeff-18
ex: Mekkora lesz a hálózat csoporterősségi együtthatója?
           { ~ 0
           ~ 0,85
           = 0,58
           ~ 0,13
           }
nk: clustering_coeff/clustering_coeff-20
""", """
serial: 21
title: transitivity_(avg)local_undirected
url: django
nk: clustering_coeff/clustering_coeff-21
note: transitivity_local_undirected(None, mode="zero") helyett lehetett volna transitivity_local_undirected(mode="zero")
""", """
serial: 22
title: karatehálózat
url: django
nk: clustering_coeff/clustering_coeff-22
ex: Mekkora lesz közelítőleg a karatehálózat csoporterősségi együtthatója?
           { ~ 0.99
           ~ 0,85
           = 0,59
           ~ 0,13
           }
so: clustering_coeff/clustering_coeff-23
""", """
serial: 24
title: karate összes csúcsra
url: django
nk: clustering_coeff/clustering_coeff-24
ex: Mekkora lesz közelítőleg a karatehálózat csoporterősségi együtthatója?
           { ~ 0.99
           ~ 0,85
           = 0,59
           ~ 0,13
           }
so: clustering_coeff/clustering_coeff-25
""", """
serial: 26
title: Szoftvercsomag hálózaté
url: django
nk: clustering_coeff/clustering_coeff-26
"""],
)


csoporterossegi_egyutthato_lecke = dict(
    serial=163,
    min_score=50,
    title="A csoporterősségi együttható",
    units=[
        dict(
            serial=1,
            max_score=5,
            title="A csoporterősségi együttható fogalma",
            items=[
                dict(
                    type="nk",
                    file_name="clustering_definition",
                ),
                dict(
                    type="qs",
                    file_name="clustering_quiz_qs",
                ),
                dict(
                    type="choice",
                    is_multiple=False,
                    text="Melyik a helyes képlet a "
                    "csoporterősségi együttható kiszámítására?",
                    answers=(
                        ("2*Ei / (ki*(ki-1))", 1),
                        ("2*Ei / (ki*(ki+1))", 0),
                        ("Ei / (ki*(ki-1))", 0),
                        ("Ei / (ki*(ki+1))", 0),
                    ),
                ),
                dict(
                    type="so",
                    file_name="clustering_quiz_so",
                ),
            ],
        ),
        dict(
            serial=2,
            max_score=5,
            title="A csoporterősségi együttható fogalma",
            items=[
                dict(
                    type="nk",
                    file_name="clustering_definition",
                ),
                dict(
                    type="qs",
                    file_name="clustering_quiz_qs",
                ),
                dict(
                    type="choice",
                    is_multiple=True,
                    text="""
                    Milyen csoporterősségű
                    csúcsok szerepelnek a hálózatban?
                    (Nyugodtan menjen vissza a videóhoz,
                    és rajzolja le a gráfot.)
                    """,
                    answers=(
                        ("1", 1/3),
                        ("1/2", 1/3),
                        ("2/5", 1/3),
                        ("1/8", -1/3),
                        ("2/7", -1/3),
                        ("2/15", -1/3),
                    ),
                ),
                dict(
                    type="so",
                    file_name="clustering_quiz_so",
                ),
            ],
        ),
        dict(
            serial=3,
            max_score=5,
            title="A csoporterősségi együtthatót kiszámító függvény",
            items=[
                dict(
                    type="qs",
                    file_name="clustering_prog_qs",
                ),
                dict(
                    type="code",
                    text="""Írjon egy függvényt clustering néven...!
                    """,
                    test=cl_unittest,
                ),
                dict(
                    type="so",
                    file_name="clustering_prog_so",
                ),
            ],
        ),
    ]
)

komponensek_atmero = dict(
    serial=100,
    min_score=50,
    title="Komponensek és átmérő",
    units=["""
serial: 2
title: Bevezető
nk: 4modul/4_3_bevezeto_v1.webm
""", """
serial: 4
title: Komponensek ismétlése
nk: 4modul/4_3_komponensek_bevezeto.webm
""", """
serial: 6
title: Webhálózat
qs: 4modul/4_3_webhalozat_qs.webm
ex: Melyikre igaz a web, mint hálózat esetén,
           hogy belőle kiválatszva két tetszőleges csúcsot,
           el tudok jutni az egyikből a másikba?
           { ~ a gyengén összefüggő komponensre
           = az erősen össszefüggő komponensre
           ~egyikre sem
           ~mindkettőre }
so: 4modul/4_3_webhalozat_so.webm
""", """
serial: 8
title: Komponensek irányítatlan hálózatban
nk: 4modul/komponensek_iranyitatlan.ogv
""", """
serial: 10
title: Erősen összefüggő komponensek
nk: 4modul/komponensek_iranyitott_eros.ogv
""", """
serial: 12
title: Gyengén összefüggő komponensek
nk: 4modul/komponensek_iranyitott_gyenge.ogv
""", """
serial: 14
title: Átmérő (irányitatlan hálózatban)
nk: 4modul/atmero_iranyitatlan.ogv
"""],
)


pylab_alapjai = dict(
    serial=44,
    min_score=50,
    public=True,
    title="A pylab használatának alapjai",
    units=["""
serial: 2
title: Bevezető és függvényábrázolás alapjai
nk: 4modul/4_4_pylab_fuggvenyfeliratok
note: "hasznos függvények: plot, xlabel, ylabel, title, legend, grid"
""", """
serial: 4
title: Tömbök létrehozása
nk: 4modul/4_4_pylab_array_letrehozasa_tipusa.ogv
note: "hasznos függvények: array, linspace, arange"
""", """
serial: 6
title: Logaritmikus skálák a tengelyeken
nk: 4modul/4_4_pylab_loglog_semilogy
note: "rajzoló függvények: plot, loglog, semilogy, semilogx"
""", """
serial: 8
title: Vonalak és markerek típusai
nk: 4modul/4_4_pylab_vonal_marker_tipusok
note: |
    formátum pl "rx--" szín: piros=Red, marker: x, vonaltípus: --=szaggatott
    lásd még: plot?
"""],
)

fokszam_eloszlas_komponensmeret = dict(
    serial=61,
    min_score=50,
    title="Fokszámok és komponensméretek",
    public=False,
    units=["""
serial: 0
title: Fokszámok meghatározása
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-00
""", """
serial: 1
title: Legnagyobb és legkisebb fokszám
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-01
""", """
serial: 2
title: Csúcsok száma
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-02
ex: Melyik függvénnyel határozhatja meg a degree metódussal kapott listából a csúcsok számát?
           { ~ range
           = len
           ~ max
           ~ min}
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-03
""", """
serial: 4
title: Élek száma
url: django
qs: igraph_fokszam_komponensek/igraph_fokszam_komponensek-04
ex: Melyik függvény segítségével határozhatja meg a degree metódussal kapott listából az élek számát?
           { = sum
           ~ len
           ~ max
           ~ min}
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-05
""", """
serial: 6
title: Adott fokszámú csúcsok száma
url: django
qs: igraph_fokszam_komponensek/igraph_fokszam_komponensek-06
ex: Melyik metódus segítségével határozhatja meg a degree metódussal kapott listából a 3 baráttal rendelkező delfinek számát?
           { ~ pop
           ~ index
           ~ append
           = count}
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-07
""", """
serial: 8
title: Fokszámgyakoriság előállítása
url: django
qs: igraph_fokszam_komponensek/igraph_fokszam_komponensek-08
ex: Hogyan kaphatjuk meg a kért listát? (Érdemes elkészíteni egy konkrét megoldást.)
           { ~%50% for ciklussal
           ~%-50% az elemek összegzésével
           ~%-50% a maximális elem megkeresésével
           ~%50% listaértelmezéssel }
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-09
""", """
serial: 10
title: Fokszámeloszlás létrehozása
url: django
qs: igraph_fokszam_komponensek/igraph_fokszam_komponensek-10
ex: Melyik metódus segítségével határozhatja meg a degree metódussal kapott listából a 3 baráttal rendelkező delfinek számát?
           { ~ pop
           ~ index
           ~ append
           = count}
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-11
""", """
serial: 13
title: Fokszámeloszlás ábrázolása
url: django
qs: igraph_fokszam_komponensek/igraph_fokszam_komponensek-13
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-14
""", """
serial: 15
title: Csomaghálózat fokszámeloszlása
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-15
""", """
serial: 16
title: Adott fokszámú csúcsok számának leolvasása loglog skáláról
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-16
ex: Mennyi a 2-es fokszámú csúcsok száma a bemutatott grafikonon?
    A saját gépén állapítsa meg, vagy térjen vissza az előző videó végéhez a navigációs sávban!
           { ~ kb. 45000
           ~ kb. 35000
           ~ kb. 3500
           = kb. 5300
           ~ kb. 530 }
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-17
""", """
serial: 18
title: Maximális fokszám leolvasása és meghatározása
url: django
qs: igraph_fokszam_komponensek/igraph_fokszam_komponensek-18
ex: Határozza meg a grafikonról, mekkora lehet az alábbiak közül a maximális fokszám? Az egyik pontos érték.
    A saját gépén állapítsa meg, vagy térjen vissza az előző videóhoz a navigációs sávban!
           { ~ 845203
           ~ 245
           ~ 1458
           = 14580
           ~ 24580 }
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-19
""", """
serial: 20
title: frequency függvény létrehozása
url: django
qs: igraph_fokszam_komponensek/igraph_fokszam_komponensek-20
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-21
""", """
serial: 22
title: Komponensek létrehozása és méretei
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-22
ex: Melyik függvény segítségével határozhatja meg a méretek listából a csúcsok számát?
           { = sum
           ~ max
           ~ len
           ~ min}
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-23
""", """
serial: 24
title: A legnagyobb komponens részaránya
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-24
ex: A legnagyobb komponens mekkora részét tartalmazza a csúcsoknak?
         { ~ 0,42
           = 0,24
           ~ 0,01
           ~ 0,51 }
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-25
""", """
serial: 26
title: Komponensméret eloszlása
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-26
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-27
""", """
serial: 28
title: Komponensek irányított hálózatban
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-28
note: |
    net.components(mode="weak")
""", """
serial: 29
title: Legnagyobb gyengén összefüggő komponens részaránya
url: django
qs: igraph_fokszam_komponensek/igraph_fokszam_komponensek-29
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-30
""", """
serial: 31
title: Gyengén összefüggő komponensek fokszámeloszlása
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-31
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-32
""", """
serial: 33
title: Erősen összefüggő komponens ábrázolása
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-33
""", """
serial: 34
title: Erősen összefüggő komponensek méreteloszlása
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-34
so: igraph_fokszam_komponensek/igraph_fokszam_komponensek-35
""", """
serial: 36
title: Összefoglalás
url: django
nk: igraph_fokszam_komponensek/igraph_fokszam_komponensek-36
"""],
)

veletlen_halozatok = dict(
    serial=62,
    min_score=50,
    title="Véletlen hálózatok",
    public=False,
    units=["""
serial: 0
title: Véletlen hálózat fogalma
url: django
nk: veletlen/veletlen_halozatok-00
qs: veletlen/veletlen_halozatok-01
ex: Hány csúcsa van egy véletlen hálózatnak?
           { ~ 2*p/(N-1)*N
           = p*(N*(N-1))/2
           ~ p*N
           ~ (2*p-1)/N}
so: veletlen/veletlen_halozatok-02
""", """
serial: 3
title: p-érték meghatározása
url: django
qs: veletlen/veletlen_halozatok-03
ex: Hogyan válasszam p értékét, hogy legjobban megközelítsem az M-et?
           { ~ 2*N/(N-1)*M
           ~ M*(N*(N-1))/2
           ~ M/(N*(N-2))
           = 2M/(N*(N-1))}
so: veletlen/veletlen_halozatok-04
""", """
serial: 5
title: Konkrét hálózat kapcsolódási valószínűsége
url: django
nk: veletlen/veletlen_halozatok-05
ex: Mekkora p érték felel meg nagyjából az asztrofizikai együttműködések
           hálózatának?
           { ~ 0,8
           ~ 0,09
           = 0,0009
           ~ 0,01
           }
so: veletlen/veletlen_halozatok-06
""", """
serial: 7
title: Átlagfokszám
url: django
nk: veletlen/veletlen_halozatok-07
ex: Mekkora az átlagfokszám az asztrofizikai együttműködések
           hálózatában?
           { ~ 1,0
           = 14,5
           ~ 9,3
           ~ 0,5
           }
so: veletlen/veletlen_halozatok-08
""", """
serial: 9
title: Hálózathoz hasonló véletlen hálózat
url: django
nk: veletlen/veletlen_halozatok-09
qs: veletlen/veletlen_halozatok-10
ex: Nagyjából mekkora az átlagfokszám az éppen előállított N=16706, p=0,00087
           paraméterű véletlen hálózatban?
           { ~ 1,0
           ~ 9,3
           ~ 20,5
           = 14,5
           }
so: veletlen/veletlen_halozatok-11
""", """
serial: 12
title: Átlagfokszám véletlen hálózatban
url: django
nk: veletlen/veletlen_halozatok-12
ex: Hogyan számítható ki a véletlen hálózat átlagfokszáma p és N ismeretében?
           { ~ 2*N/((N-1)*p)
           ~ p*(N-1)/2
           = p*(N-1)
           ~ p/(N*(N-1))}
so: veletlen/veletlen_halozatok-13
""", """
serial: 14
title: Átlagfokszám
url: django
nk: veletlen/veletlen_halozatok-14
""", """
serial: 16
title: Fokszámeloszlás létrehozása
url: django
nk: veletlen/veletlen_halozatok-16
# ex: Írja meg a programot....
so: veletlen/veletlen_halozatok-17
""", """
serial: 19
title: Fokszámeloszlás ábrázolása
url: django
nk: veletlen/veletlen_halozatok-19
so: veletlen/veletlen_halozatok-20
""", """
serial: 21
title: Az Erdos_Renyi osztálymetódus részletei
url: django
nk: veletlen/veletlen_halozatok-21
""", """
serial: 22
title: Összefoglalás
url: django
nk: veletlen/veletlen_halozatok-22
"""],
)


oh_bevezetes = dict(
    serial=0,
    min_score=50,
    title="Bevezetés",
    public=True,
    units=["""
serial: 1
title: Általános tudnivalók
nk: altalanos/OHV_bevezeto
note: '<a href="http://kmooc.uni-obuda.hu">A KMOOC honlapja</a> itt kell regisztrálni.<br> <a href="http://privat.amk.uni-obuda.hu/~horvatha/elerhetoseg.html">Horváth Árpád elérhetőségei</a>'
""", """
serial: 2
title: A kurzus tananyaga
nk: altalanos/OHV_tananyagrol
note: '<a href="http://django.amk.uni-obuda.hu/segedletek/oh/Osszetett_halozatok_vizsgalata_kovetelmenyrendszer.pdf">A tananyag részletes leírása.</a> <br> <a href="http://django.amk.uni-obuda.hu/segedletek/oh/2016osz_Osszetett_halozatok_vizsgalata_eKurzus.pdf">Követelmények a tárgy teljesítéséhez, határidők</a>'
""", """
serial: 3
title: A kurzushoz tartozó szoftverek telepítése
nk: altalanos/OHV_szoftvertelepites
note: "A KMOOC-on bővebben lesz róla."
""", """
serial: 4
title: Kapcsolattartás, kérdések feltevése
nk: altalanos/OHV_konzultacio2016
note: "A KMOOC-on bővebben lesz róla."
"""],
)

# oh_course_intro_dict = dict(
#     title="Az összetett hálózatok vizsgálata tudnivalók",
#     description="""
#     Az Internet, az állatok idegrendszere, az emberek közötti baráti kapcsolatok
#     mindegyik példa egy hálózatra.
#     Bár a tudományterület eltérő, sokszor hasonló fogalmakra van szükségük.
#
#     Az itt található videók a kurzussal kapcsolatos tudnivalókat tartalmazzák.
#     """,
#     lessons=[oh_bevezetes]
#     )

kiserleti_kurzus_text = """
    Kísérleti kurzus. Az itt levő leckék a pyEdu fejlesztését
    szolgálják, nincs hatékony tanulásra összeállítva. Természetesen nem
    titkos, csak időpazarlás.
    """

test_net_course_dict = dict(
    title="Tesztkurzus hálózatokról",
    description=kiserleti_kurzus_text + """
    Az Internet, az állatok idegrendszere, az emberek közötti baráti kapcsolatok
    mindegyik példa egy hálózatra.
    Bár a tudományterület eltérő, sokszor hasonló fogalmakra van szükségük.""",
    lessons=[
        dict(
            serial=2,
            min_score=50,
            title="Bevezetés a Python nyelvbe",
            units=[
                dict(
                    serial=1,
                    max_score=5,
                    title="Szótár létrehozása",
                    items=[
                        dict(
                            type="nk",
                            file_name=test_video_name,
                        ),
                        dict(
                            type="qs",
                            file_name=test_video_name,
                        ),
                        dict(
                            type="choice",
                            is_multiple=True,
                            text="Melyik módon hozhatunk létre "
                            "szótárat Pythonban?",
                            answers=(
                                ("{'a': 4}", 0.5),
                                ("{'a', 'b', 'c'}", -0.5),
                                ("dict(a=4)", 0.5),
                                ("{a:4}", -0.5),
                            ),
                        ),
                        dict(
                            type="so",
                            file_name=test_video_name,
                            url="http://localhost/media/",
                        ),
                    ],
                ),
            ]
        ),
        dict(
            serial=1,
            min_score=50,
            title="Alapfogalmak"
        ),
        dict(
            serial=50,
            min_score=50,
            title="Az igraph használata"
        ),
        dict(
            serial=60,
            min_score=50,
            title="A csoporterősségi együttható",
            units=[
                dict(
                    serial=1,
                    max_score=5,
                    title="A csoporterősségi együttható fogalma",
                    items=[
                        dict(
                            type="nk",
                            file_name="clustering_definition",
                        ),
                        dict(
                            type="qs",
                            file_name="clustering_quiz_qs",
                        ),
                        dict(
                            type="choice",
                            is_multiple=False,
                            text="Melyik a helyes képlet a "
                            "csoporterősségi együttható kiszámítására?",
                            answers=(
                                ("2*Ei / (ki*(ki-1))", 1),
                                ("2*Ei / (ki*(ki+1))", 0),
                                ("Ei / (ki*(ki-1))", 0),
                                ("Ei / (ki*(ki+1))", 0),
                            ),
                        ),
                        dict(
                            type="so",
                            file_name="clustering_quiz_so",
                        ),
                    ],
                ),
                dict(
                    serial=2,
                    max_score=5,
                    title="A csoporterősségi együttható fogalma",
                    items=[
                        dict(
                            type="nk",
                            file_name="clustering_definition",
                        ),
                        dict(
                            type="qs",
                            file_name="clustering_quiz_qs",
                        ),
                        dict(
                            type="choice",
                            is_multiple=True,
                            text="""
                            Milyen csoporterősségű
                            csúcsok szerepelnek a hálózatban?
                            (Nyugodtan menjen vissza a videóhoz,
                            és rajzolja le a gráfot.)
                            """,
                            answers=(
                                ("1", 1/3),
                                ("1/2", 1/3),
                                ("2/5", 1/3),
                                ("1/8", -1/3),
                                ("2/7", -1/3),
                                ("2/15", -1/3),
                            ),
                        ),
                        dict(
                            type="so",
                            file_name="clustering_quiz_so",
                        ),
                    ],
                ),
                dict(
                    serial=3,
                    max_score=5,
                    title="A csoporterősségi együtthatót kiszámító függvény",
                    items=[
                        dict(
                            type="qs",
                            file_name="clustering_prog_qs",
                        ),
                        dict(
                            type="code",
                            text="""Írjon egy függvényt clustering néven...!
                            """,
                            test=cl_unittest,
                        ),
                        dict(
                            type="so",
                            file_name="clustering_prog_so",
                        ),
                    ],
                ),
            ]
        ),
    ]
    )

lessons = [
    oh_bevezetes,
    # alapok,
    # python_bevezeto,
    komponensek_atmero,
    pylab_alapjai,
    fokszam_eloszlas_komponensmeret,
    veletlen_halozatok,
    clustering_coeff,
    # csoporterossegi_egyutthato_lecke,
]

oh_course_dict = dict(
    title="Az összetett hálózatok vizsgálata",
    description="""
    Az Internet, az állatok idegrendszere, az emberek közötti baráti kapcsolatok
    mindegyik példa egy hálózatra.
    Bár a tudományterület eltérő, sokszor hasonló fogalmakra van szükségük.

    A jelenlegi kurzus bemutatja az összetett hálózatok alapvető fogalmait, és
    azok vizsgálatát a Python programozási nyelv iGraph nevű moduljának a
    használatával.

    Jelenleg a kurzus egyik része Moodle rendszerben érhető el, míg egyre
    nagyobb része a pyEdu oldalon.
    """,
    lessons=lessons
    )
