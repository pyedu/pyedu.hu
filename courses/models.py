from django.db import models
from django.contrib import auth
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse

video_types = ['nk', 'qs', 'so']
item_types = ['nk', 'qs', 'ex', 'so']

END_OF_COURSE = None


def mapping_to_next(sequence, last_item=None):
    mapping_to_next = {
        sequence[i]: sequence[i+1]
        for i in range(len(sequence)-1)
    }
    mapping_to_next[sequence[-1]] = last_item
    return mapping_to_next


def next_element(self, element_name, serial):
    element_set = eval("self.{}_set".format(element_name))
    next_elements = element_set.filter(
        serial__gt=serial
    ).order_by('serial')
    if next_elements:
        return next_elements[0]
    return None


class User(auth.models.User):
    class Meta:
        proxy = True

    def sum_one_course_scores(self, course):
        scores = self.score_set.filter(unit__lesson__course=course)
        return sum(score.score for score in scores)

    def get_scores(self):
        all_courses_scores = []
        for course in Course.objects.all():
            scores = self.sum_one_course_scores(course)
            if scores:
                all_courses_scores.append((course, scores))
        return all_courses_scores

    def get_name(self):
        first_name = self.first_name
        last_name = self.last_name
        if first_name or last_name:
            return " ".join([last_name, first_name])
        else:
            return self.username


# class UserProfile(models.Model):
#    picture = models.ImageField(upload_to='profile_images', blank=True)
#
#    def __str__(self):
#        return self.user.username


class Course(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(max_length=500)
    min_score = models.IntegerField(default=0)

    class Meta:
        ordering = ["title"]

    def __str__(self):
        return self.title

    def next_lesson(self, lesson):
        return next_element(self, "lesson", lesson.serial)

    def next_not_empty_lesson(self, lesson):
        next_lesson = lesson
        while lesson is not None:
            next_lesson = self.next_lesson(next_lesson)
            if next_lesson is None:
                return None
            elif next_lesson.has_item():
                return next_lesson

    def get_lesson_by_serial_or_title(self, serial_or_title):
        try:
            if isinstance(serial_or_title, str):
                lesson = self.lesson_set.get(title=serial_or_title)
            else:
                lesson = self.lesson_set.get(serial=serial_or_title)
        except ObjectDoesNotExist:
            return None
        return lesson

    def get_element(self, *args):
        args = list(args)
        lesson = self.get_lesson_by_serial_or_title(args.pop(0))
        if not args or lesson is None:
            return lesson
        unit = lesson.get_unit_by_serial_or_title(args.pop(0))
        if not args or unit is None:
            return unit
        type_ = args.pop(0)
        assert not args
        return unit.get_item(type_)

    def get_exercises(self):
        exercises = []
        for lesson in self.lesson_set.all():
            for unit in lesson.unit_set.all():
                exercise = unit.get_exercise()
                if exercise:
                    exercises.append(exercise)
        return exercises

    def max_score(self):
        return sum(exercise.unit.max_score for exercise in self.get_exercises())

    def active_users_number(self):
        users = User.objects.all()
        return [u.sum_one_course_scores(self) == 0 for u in users].count(False)


class Lesson(models.Model):
    title = models.CharField(max_length=100)
    course = models.ForeignKey(Course)
    serial = models.IntegerField()
    min_score = models.IntegerField()
    public = models.BooleanField(default=False)

    class Meta:
        ordering = ["course", "serial"]

    def __str__(self):
        return self.title

    def next_unit(self, unit):
        return next_element(self, "unit", unit.serial)

    def first_unit(self):
        unit_set = self.unit_set.order_by('serial')
        if unit_set:
            return unit_set[0]

    def get_unit_by_serial_or_title(self, serial_or_title):
        try:
            if isinstance(serial_or_title, str):
                unit = self.unit_set.get(title=serial_or_title)
            else:
                unit = self.unit_set.get(serial=serial_or_title)
        except ObjectDoesNotExist:
            return None
        return unit

    def next_lesson_first_item(self):
        next_not_empty_lesson = self.course.next_not_empty_lesson(self)
        if next_not_empty_lesson:
            unit = next_not_empty_lesson.first_unit()
            if unit:
                first_item = unit.first_item()
                if first_item:
                    return unit.first_item()
        return END_OF_COURSE

    def has_item(self):
        for unit in self.unit_set.all():
            if unit.has_item():
                return True
        return False


def is_lesson_serial_in_course(course, serial):
    return bool(course.lesson_set.objects.get(serial=serial))


class Unit(models.Model):
    title = models.CharField(max_length=100, default="")
    lesson = models.ForeignKey(Lesson)
    serial = models.IntegerField()
    max_score = models.IntegerField()
    note = models.TextField(max_length=300, default="")

    class Meta:
        ordering = ["lesson__course", "lesson", "serial"]
        unique_together = (("lesson", "serial"),)

    def __str__(self):
        return self.title or "Untitled unit"

    def get_absolute_url(self):
        return reverse(
            'unit',
            args=(str(i) for i in
                  (self.lesson.course.id,
                   self.lesson.serial,
                   self.serial)
                  )
        )

    def get_video(self, type_):
        video_set = self.video_set.filter(type=type_)
        if video_set:
            return video_set[0]

    def get_exercise(self):
        try:
            return self.exercise
        except ObjectDoesNotExist:
            return None

    def has_exercise(self):
        return self.get_exercise() is not None

    def get_item(self, type_):
        if type_ in video_types:
            return self.get_video(type_)
        elif type_ == 'ex':
            return self.get_exercise()
        else:
            raise TypeError('Item can not have type {}'.format(type_))

    def next_item(self, item):
        type_index = item_types.index(item.type)
        if type_index == len(item_types):
            return None
        for type_ in item_types[type_index+1:]:
            item = self.get_item(type_)
            if item:
                return item

    def first_item(self):
        for type_ in item_types:
            item = self.get_item(type_)
            if item:
                return item

    def has_item(self):
        return self.first_item() is not None

    def first_item_of_next_unit(self):
        lesson = self.lesson
        next_unit = lesson.next_unit(self)
        if next_unit:
            return next_unit.first_item()
        return lesson.next_lesson_first_item()

    def is_public(self):
        return self.lesson.public


def next_item(item):
    unit = item.unit
    next_item = unit.next_item(item)
    if next_item:
        return next_item
    return unit.first_item_of_next_unit()


class Score(models.Model):
    unit = models.ForeignKey(Unit)
    user = models.ForeignKey(User)
    score = models.FloatField()
    answer = models.CharField(max_length=500)
