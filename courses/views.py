from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from courses.models import Course, Lesson, User
from django.contrib.auth import logout
import os


def index(request):
    courses = Course.objects.all().order_by('title')
    context = {'courses': courses}
    return render(request, 'courses/index.html', context)


def course_detail(request, id):
    course = Course.objects.get(id=id)
    lessons = course.lesson_set.order_by("serial")
    return render(request, 'courses/course_details.html',
                  {"course": course, "lessons": lessons})


def lesson_detail(request, course_id, lesson_serial):
    lesson = Lesson.objects.get(serial=lesson_serial, course__id=course_id)
    unit = lesson.first_unit()
    return render(
        request, 'courses/lesson_details.html',
        {
            "unit": unit,
            "next_item": unit.first_item()
        }
    )


def user_logout(request):
    logout(request)
    return redirect('courses_index')


@login_required
def score(request):
    user = get_user(request.user)
    return render(request, 'courses/score.html', {"user": user})


def end_of_course(request, course_id):
    return render(
        request,
        'courses/end_of_course.html',
        {'course': Course.objects.get(id=course_id)}
    )


from django.template.loader import get_template
from django.template import Context
from pyEdu.settings import MEDIA_ROOT
DIPLOMA_DIR = os.path.join(MEDIA_ROOT, 'diploma')


def save_tex_diploma(user):
    template = get_template('courses/diploma.tex')
    rendered = template.render(
        Context({'user': user})
    )
    tex_file = os.path.join(
        DIPLOMA_DIR, "diploma_{}.tex").format(user.username)
    with open(tex_file, "w", encoding='utf-8') as f:
        f.write(rendered)
    return tex_file


def pdflatex(filename):
    current_directory = os.getcwd()
    os.chdir(DIPLOMA_DIR)
    os.system('pdflatex {}'.format(filename))
    os.chdir(current_directory)


def get_diploma(request):
    tex_file = save_tex_diploma(get_user(request.user))
    pdflatex(tex_file)
    # time.sleep(1)
    return redirect('score')


def get_user(auth_user):
    username = auth_user.username
    return User.objects.get(username=username)
