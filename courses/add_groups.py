from django.contrib import auth


def add_groups(*groups):
    for group in groups:
        auth.models.Group.objects.create(name=group)


def add_base_groups():
    add_groups("students", "teachers")
