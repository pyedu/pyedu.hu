from django.contrib import admin
from courses.models import Course, Lesson, Unit


class LessonInline(admin.TabularInline):
    model = Lesson
    extra = 2


class CourseAdmin(admin.ModelAdmin):
    inlines = [LessonInline]


admin.site.register(Course, CourseAdmin)
admin.site.register(Unit)
