from django.conf.urls import url
from django.views.generic import TemplateView

from courses import views
from videos.views import item, unit
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^$', views.index, name='courses_index'),
    url(r'^(\d+)/$', views.course_detail, name='course_detail'),
    url(r'^(\d+)/(\d+)/$', views.lesson_detail, name='lesson_detail'),
    url(r'^score/$', views.score, name='score'),
    url(r'^score/get_diploma/$', views.get_diploma, name='get_diploma'),
    url(r'^login/$', auth_views.login,
        {'template_name': 'courses/login.html'}, name='login'),
    url(r'^logout/$', views.user_logout, name='logout'),
    url(r'^contact/$', TemplateView.as_view(template_name="contact.html"),
        name="contact"),
    url(r'^about/$', TemplateView.as_view(template_name="pyEdu/about.html"),
        name="about"),
    url(r'^(\d+)/end/$', views.end_of_course, name='end_of_course'),
    url(r'^(\d+)/(\d+)/(\d+)/(nk|qs|so|ex)/$',
        item, name='item'),
    url(r'^(\d+)/(\d+)/(\d+)/$', unit, name='unit'),
]
