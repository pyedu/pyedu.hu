import courses
from .base import CourseTestCase
from courses.models import Unit, END_OF_COURSE
from courses.models import mapping_to_next
from courses.create_course import formal_languages_item_list


class TestNavigationFunctions(CourseTestCase):
    def test_next_item_return_proper_values(self):
        known_values = mapping_to_next(formal_languages_item_list)
        for item_args, next_item_args in known_values.items():
            item = self.course.get_element(*item_args)
            next_item = None if next_item_args is None\
                else self.course.get_element(*next_item_args)
            self.assertEqual(courses.models.next_item(item),
                             next_item)

    def test_next_item_return_proper_values_OLD(self):
        units = self.units
        unit = units[21]
        item1 = unit.video_set.get(type='nk')
        item2 = unit.exercise
        item3 = unit.video_set.get(type='so')
        next_unit_first_item = self.course.get_element(5, 2, 'nk')
        self.assertEqual(courses.models.next_item(item1),
                         item2)
        self.assertEqual(courses.models.next_item(item2),
                         item3)
        self.assertEqual(courses.models.next_item(item3),
                         next_unit_first_item)


class TestMethodsOfLessonClass(CourseTestCase):
    def test_next_unit_gives_proper_value(self):
        units = self.units
        known_values = mapping_to_next((
            units[1],
            units[10],
            units[19],
            units[21],),
        )
        for unit, next_unit in known_values.items():
            self.assertEqual(self.lessons[2].next_unit(unit), next_unit)

    def test_first_unit_gives_proper_value(self):
        course = self.course
        known_values = {1: 2, 2: 1, 5: 2}
        for lesson_serial, unit_serial in known_values.items():
            first_unit = course.get_element(lesson_serial, unit_serial)
            lesson = first_unit.lesson
            self.assertEqual(lesson.first_unit(), first_unit)

    def test_next_lesson_first_item_return_proper_values(self):
        lessons = self.lessons
        known_values = {
            lessons[i]: lessons[j].first_unit().first_item()
            for i, j in ((1, 2), (2, 5))
        }
        for lesson, next_lesson_first_item in known_values.items():
            self.assertEqual(
                lesson.next_lesson_first_item(),
                next_lesson_first_item,
            )
        for lesson_serial in (5, 99):
            lesson = lessons[lesson_serial]
            self.assertEqual(
                lesson.next_lesson_first_item(),
                END_OF_COURSE
            )


class TestMethodsOfUnitClass(CourseTestCase):
    def test_next_lesson_gives_proper_value(self):
        lessons = self.lessons
        known_values = mapping_to_next(
            (lessons[1],
             lessons[2],
             lessons[3],
             lessons[5],
             lessons[99]
             ),
        )
        for lesson, next_lesson in known_values.items():
            self.assertEqual(self.course.next_lesson(lesson), next_lesson)

    def test_next_not_empty_lesson_gives_proper_value(self):
        lessons = self.lessons
        known_values = mapping_to_next(
            (lessons[1],
             lessons[2],
             lessons[5],
             ),
        )
        for lesson, next_lesson in known_values.items():
            self.assertEqual(
                self.course.next_not_empty_lesson(lesson),
                next_lesson
            )

    def test_next_item_gives_proper_value(self):
        long_unit = self.units[21]
        known_values = mapping_to_next(("nk", "ex", "so"))
        for item_type, next_item_type in known_values.items():
            item = long_unit.get_item(item_type)
            next_item = long_unit.next_item(item)
            if next_item:
                self.assertEqual(next_item.type, next_item_type)
            else:
                self.assertEqual(next_item, None)

    def test_first_item_of_next_unit_return_proper_values(self):
        units = self.units
        self.assertEqual(
            units[1].first_item_of_next_unit(),
            units[10].video_set.get(type='nk')
        )

    def test_first_item_of_next_unit_return_proper_values_if_no_next_unit_in_lesson(self):
        units = self.units
        course = self.course
        item = course.get_element(5, 2, 'nk')
        self.assertEqual(
            units[21].first_item_of_next_unit(),
            item
        )


class TestMethodsOfCourseClass(CourseTestCase):
    def test_course_can_get_element(self):
        for item_args in formal_languages_item_list:
            item = self.course.get_element(*item_args)
            self.assertIsInstance(item.unit, Unit)

    def test_get_element_returns_None_for_not_existing_elements(self):
        not_existing_element_list = (
            (1, 3, 'qs'),
            (2, 2, 'ex'),
            (1, 2, 'nk'),
            (2, 1, 'so'),
            (2, 10, 'qs'),
            (2, 19, 'ex'),
            (2, 21, 'qs'),
            (5, 2, 'so'),
            (1, 3),
            (3, 5),
            (4, ),
            (4, 2),
            (4, 2, 'nk'),
            (7, ),
        )
        for element_args in not_existing_element_list:
            element = self.course.get_element(*element_args)
            self.assertEqual(element, None)
