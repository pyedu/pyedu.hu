import unittest
from courses.halozatok import create_units_from_string
from courses.create_course import create_unit_dict_from_string
from courses.add_groups import add_groups
from exercises.gift import create_choice_dict_from_gift
from courses.halozatok import remove_video_extension
from courses import halozatok
from django.contrib import auth

unit_string = """
serial: 3
title: French revolution
nk: French_revolution
qs: French_revolution_question
ex: "::French rev. date:: When was the French revolution?
      { ~1678 =1789 ~1777 ~1765 }"
so: French_revolution_solution
"""

unit_dict = dict(
    serial=3,
    items=[
        dict(file_name="French_revolution", type='nk'),
        dict(file_name="French_revolution_question", type='qs'),
        dict(file_name="French_revolution_solution", type='so'),
        dict(
            type="choice",
            is_multiple=False,
            text="When was the French revolution?",
            answers=(
                ("1678", 0.),
                ("1789", 1.),
                ("1777", 0.),
                ("1765", 0.),
            ),
        ),
    ],
    title="French revolution",
)

string = """
 scc_egy_komponens_meghatarozasa,Egy erősen összefüggő komponens meghatározása
     scc_komponensekre_bontas,Komponensekre bontás
scc_komponenshatarnal_megallas,Miért állhatunk meg komponenshatárnál
"""

unit_list = [
    {'items':
     [{'file_name': 'scc_egy_komponens_meghatarozasa',
       'type': 'nk'}],
     'serial': 1,
     'title':
     'Egy erősen összefüggő komponens meghatározása'},
    {'items':
     [{'file_name': 'scc_komponensekre_bontas',
       'type': 'nk'}],
     'serial': 2,
     'title':
     'Komponensekre bontás'},
    {'items':
     [{'file_name': 'scc_komponenshatarnal_megallas',
       'type': 'nk'}],
     'serial': 3,
     'title': 'Miért állhatunk meg komponenshatárnál'}
]

unit_list_with_mixed_serials = [
    {'items':
     [{'file_name': 'scc_egy_komponens_meghatarozasa',
       'type': 'nk'}],
     'serial': 31,
     'title':
     'Egy erősen összefüggő komponens meghatározása'},
    {'items':
     [{'file_name': 'scc_komponensekre_bontas',
       'type': 'nk'}],
     'serial': 26,
     'title':
     'Komponensekre bontás'},
    {'items':
     [{'file_name': 'scc_komponenshatarnal_megallas',
       'type': 'nk'}],
     'serial': 3,
     'title': 'Miért állhatunk meg komponenshatárnál'}
]


class TestCreateUnitsFromString(unittest.TestCase):
    def test_create_units_from_string_return_proper_value(self):
        self.assertEqual(
            create_units_from_string(string), unit_list
        )


class TestCreateUnitDictFromString(unittest.TestCase):
    def test_create_unit_dict_from_string_return_proper_value(self):
        self.maxDiff = None
        self.assertEqual(
            create_unit_dict_from_string(unit_string), unit_dict
        )


class TestCreateChoiceFromGIFT(unittest.TestCase):
    def test_choice_from_dict_and_from_dict_is_same(self):
        choice_dict = dict(
            type="choice",
            is_multiple=True,
            text="What two people are entombed in Grant's tomb?",
            answers=(
                ("No one", -1),
                ("Grant", 0.5),
                ("Grant's wife", 0.5),
                ("Grant's father", -1),
            ),
        )
        choice_gift = """What two people are entombed in Grant's tomb? {
        ~%-100%No one
        ~%50%Grant
        ~%50%Grant's wife
        ~%-100%Grant's father
        }"""
        self.assertEqual(create_choice_dict_from_gift(choice_gift), choice_dict)


class TestGetMaxUnitSerial(unittest.TestCase):
    def setUp(self):
        self.known_values = (
            (unit_list, 3),
            (unit_list_with_mixed_serials, 31),
        )

    def test_next_lesson_gives_proper_value(self):
        for unit_list, value in self.known_values:
            self.assertEqual(
                halozatok.get_max_unit_serial(unit_list), value
            )


class TestRemoveVideoExtension(unittest.TestCase):
    def test_remove_video_extension(self):
        known_results = (
            ("x.ogv", "x"),
            ("x", "x"),
            ("x.jpg", "x.jpg"),
            ("Hosszu_nevu_fajl.ogv", "Hosszu_nevu_fajl"),
            ("Hosszu_nevu_fajl", "Hosszu_nevu_fajl"),
            ("Hosszu_nevu_fajl.jpg", "Hosszu_nevu_fajl.jpg"),
            ("x.webm", "x"),
            ("x.mp4", "x"),
            ("x.WebM", "x"),
        )
        for video, result in known_results:
            self.assertEqual(remove_video_extension(video), result)


class TestCreateUsersAndGroups(unittest.TestCase):
    def test_add_groups(self):
        groups = ("group1", "group2")
        add_groups(*groups)
        for group in groups:
            self.assertEqual(len(auth.models.Group.objects.filter(name=group)),
                             1)
