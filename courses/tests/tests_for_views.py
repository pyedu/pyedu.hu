from django.core.urlresolvers import resolve, reverse
from django.template.loader import render_to_string
from django.http import HttpRequest
from django.test import TestCase
from unittest import skip
from courses.views import index, course_detail
from courses.models import Course
from courses.add_groups import add_base_groups

from exercises.views import save_score_and_answer
from courses.create_course import (
    create_course_from_dict,
    formal_languages_course_dict,
    test_net_course_dict,
    add_students,
    create_unit_from_dict,
)

unit_dict_with_note = dict(
    serial=3,
    items=[
        dict(file_name="French_revolution", type='nk'),
        dict(file_name="French_revolution_question", type='qs'),
        dict(file_name="French_revolution_solution", type='so'),
        dict(
            type="choice",
            is_multiple=False,
            text="When started the French revolution?",
            answers=(
                ("1678", 0.),
                ("1789", 1.),
                ("1777", 0.),
                ("1765", 0.),
            ),
        ),
    ],
    title="French revolution",
    note="This is a note",
)


def set_max_score(unit, user):
    save_score_and_answer(user.username, unit, unit.max_score, 'answer')


class ScoreTest(TestCase):
    def setUp(self):
        course1 = create_course_from_dict(formal_languages_course_dict)
        course2 = create_course_from_dict(test_net_course_dict)
        add_base_groups()
        user1, user2 = add_students("diak", "diak2")

        units = (
            (user1, course1, 1, 2),  # 5
            (user1, course1, 2, 21),  # 5
            (user1, course1, 5, 2),  # 5

            (user2, course1, 1, 2),  # 5

            (user1, course2, 60, 1),  # 5
            (user1, course2, 60, 2),  # 5
        )
        for user, course, lesson_serial, unit_serial in units:
            unit = course.get_element(lesson_serial, unit_serial)
            if unit is None:
                print(course, lesson_serial, unit_serial)
            set_max_score(unit, user)
        self.known_values = {
            user1: {
                course1: 15,
                course2: 10,
            },
            user2: {
                course1: 5,
                course2: 0,
            },
        }

        self.course_max_score = {
            course1: 15,
        }

    def test_course_max_score_returns_proper_values(self):
        for course, max_score in self.course_max_score.items():
            self.assertEqual(course.max_score(), max_score)

    def test_sum_all_course_scores_method_returns_the_proper_values(self):
        for user, course_dict in self.known_values.items():
            scores = user.get_scores()
            for course, score in course_dict.items():
                if score:
                    self.assertIn((course, score), scores)
                else:
                    self.assertNotIn((course, score), scores)

    def test_sum_one_course_scores_method_returns_the_proper_values(self):
        for user, course_dict in self.known_values.items():
            for course, score in course_dict.items():
                self.assertEqual(user.sum_one_course_scores(course), score)


def create_a_course_with_one_empty_lesson():
    course = Course.objects.create(
        title="Course with one empty lesson",
        description="",
    )
    lesson = course.lesson_set.create(
        title="Empty lesson",
        serial=7,
        min_score=55
    )
    return lesson


class TestCreateUnitFromDict(TestCase):
    def setUp(self):
        self.lesson = create_a_course_with_one_empty_lesson()

    def test_create_unit_from_dict_stores_the_proper_values(self):
        unit = create_unit_from_dict(unit_dict_with_note, self.lesson)
        course = unit.lesson.course
        unit_serial, lesson_serial = unit.serial, unit.lesson.serial
        self.assertEqual(
            course.get_element(lesson_serial, unit_serial).note,
            unit_dict_with_note["note"]
        )


class TestNoteInPages(TestCase):
    def setUp(self):
        self.lesson = create_a_course_with_one_empty_lesson()
        add_base_groups()
        user1, user2 = add_students("diak", "diak2")
        # self.client.login(user="diak", password="diak")

    # @skip("Bug. I don't know why is the post method needed")
    def test_create_unit_from_dict_stores_the_proper_values(self):
        create_unit_from_dict(unit_dict_with_note, self.lesson)
        response = self.client.post('/course/login/?next=/course/1/7/3/ex/', {'username': 'diak', 'password': 'diak'})
        response = self.client.get(reverse("item", args=(1, 7, 3, 'ex')), follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, "A kurzusok megtekintéséhez")
        self.assertContains(response, "note")


class SourceHomePageTest(TestCase):
    def test_courses_url_resolves_index_page_view(self):
        found = resolve("/course/")
        self.assertEqual(found.func, index)

    def test_home_page_returns_correct_html(self):
        response = index(HttpRequest())
        courses = Course.objects.all().order_by("title")
        expected_html = render_to_string('courses/index.html',
                                         {"courses": courses})
        self.assertEqual(response.content.decode(), expected_html)


class SourceDetailPageTest(TestCase):
    def test_courses_url_resolves_index_page_view(self):
        found = resolve("/course/1/")
        self.assertEqual(found.func, course_detail)
