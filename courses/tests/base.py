from django.test import TestCase
import re
from courses.models import Course
from courses.create_course import (create_course_from_dict,
                                   formal_languages_course_dict,
                                   )


class CourseTestCase(TestCase):
    def setUp(self):
        create_course_from_dict(formal_languages_course_dict)
        self.course = Course.objects.get(pk=1)
        self.lessons = self.get_lessons()
        self.units = self.get_units(lesson_serial=2)

    def get_elements_sorted_by_serial(self, element_set):
        sorted_element_serials = sorted([
            element.serial for element in element_set.all()
        ])
        return {
            i: element_set.get(serial=i)
            for i in sorted_element_serials
        }

    def get_lessons(self):
        return self.get_elements_sorted_by_serial(self.course.lesson_set)

    def get_units(self, lesson_serial):
        lesson = self.lessons[lesson_serial]
        return self.get_elements_sorted_by_serial(lesson.unit_set)

    @staticmethod
    def remove_csfr(html_code):
        csrf_regex = r'<input[^>]+csrfmiddlewaretoken[^>]+>'
        return re.sub(csrf_regex, '', html_code)

    def assertEqualExceptCSFR(self, html_code1, html_code2):
        return self.assertEqual(
            self.remove_csfr(html_code1),
            self.remove_csfr(html_code2)
        )
