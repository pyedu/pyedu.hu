from django.test import TestCase
from courses.models import Course, mapping_to_next, User
from courses.create_course import (
    create_course_from_dict,
    formal_languages_course_dict,
    # oh_course_dict,
    # formal_languages_item_list,
    add_student,
)
from courses.add_groups import add_base_groups


class ModelTestCase(TestCase):
    def setUp(self):
        self.course = create_course_from_dict(formal_languages_course_dict)
        add_base_groups()
        self.student1 = add_student('student1')
        self.student2 = add_student('student2')


def get_lesson(course_title, lesson_title):
    course = Course.objects.get(title=course_title)
    return course.lesson_set.get(title=lesson_title)


class MappingToNextTest(TestCase):

    def test_mapping_to_next_returns_proper_values(self):
        known_values = (
            (
                [1, 2, 6, 7],
                {1: 2, 2: 6, 6: 7, 7: None}
            ),
            (
                ("nk qs ex so".split()),
                {"nk": "qs", "qs": "ex", "ex": "so", "so": None}
            ),
        )
        for sequence, mapping in known_values:
            self.assertEqual(mapping_to_next(sequence), mapping)


class UnitModelTest(ModelTestCase):

    def test_unit_can_be_created_with_title(self):
        lesson = get_lesson('Tesztkurzus formális nyelvekről', 'Levezetési fa')
        unit = lesson.unit_set.create(
            lesson=lesson,
            serial=77,
            max_score=4,
            title='Unit 77'
        )
        self.assertEqual(unit.serial, 77)
        self.assertEqual(lesson.unit_set.count(), 2)

    def test_unit_can_be_created_without_title(self):
        lesson = get_lesson('Tesztkurzus formális nyelvekről', 'Levezetési fa')
        unit = lesson.unit_set.create(
            lesson=lesson,
            serial=77,
            max_score=4,
        )
        self.assertEqual(unit.title, '')
        self.assertEqual(lesson.unit_set.count(), 2)

    def test_unit_can_get_item(self):
        lesson = get_lesson('Tesztkurzus formális nyelvekről',
                            'Véges automaták')
        unit = lesson.unit_set.get(serial=21)
        for video_type in "nk qs so".split():
            video = unit.get_item(video_type)
            self.assertEqual(video is None, video_type == 'qs')
        exercise = unit.get_item('ex')
        self.assertEqual(exercise.choice.is_multiple, False)

    def test_unit_can_get_video(self):
        lesson = get_lesson('Tesztkurzus formális nyelvekről',
                            'Véges automaták')
        unit = lesson.unit_set.get(serial=21)
        for video_type in "nk qs so".split():
            video = unit.get_video(video_type)
            self.assertEqual(video is None, video_type == 'qs')

    def test_unit_can_get_first_item(self):
        known_values = (
            (('Véges automaták', 21), 'nk'),
            (('Reguláris kifejezések', 2), 'qs'),
        )
        for data, item_type in known_values:
            lesson_name, unit_serial = data
            lesson = get_lesson('Tesztkurzus formális nyelvekről', lesson_name)
            unit = lesson.unit_set.get(serial=unit_serial)
            first_item = unit.first_item()
            self.assertEqual(first_item.type, item_type)


class CourseModelTest(ModelTestCase):

    def test_course_can_get_element(self):
        lesson = get_lesson('Tesztkurzus formális nyelvekről',
                            'Reguláris kifejezések')
        course = lesson.course
        unit = lesson.unit_set.get(serial=2)
        item = unit.get_item('nk')
        self.assertEqual(course.get_element(1, 2), unit)
        self.assertEqual(course.get_element(1, 2, 'nk'), item)


class UserModelTest(ModelTestCase):

    def test_user_can_be_created_with_first_and_last_name(self):
        self.student1.first_name = 'Rowan'
        self.student1.last_name = 'Atkinson'
        self.student1.save()
        st = User.objects.get(username='student1')
        self.assertEqual(st.first_name, 'Rowan')
        self.assertEqual(st.last_name, 'Atkinson')

    def test_get_name_working_properly(self):
        self.assertEqual(self.student1.get_name(), 'student1')
        self.student1.first_name = 'Béla'
        self.student1.last_name = 'Bartók'
        self.student1.save()
        self.assertEqual(self.student1.get_name(), 'Bartók Béla')
