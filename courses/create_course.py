from courses.models import Course, User
from django.contrib import auth
import exercises
from textwrap import dedent
from yaml import load
from exercises.gift import create_choice_dict_from_gift
from courses.halozatok import (test_net_course_dict, oh_course_dict,
                               kiserleti_kurzus_text,
                               remove_video_extension,
                               test_video_name)
from .add_groups import add_base_groups
from .passwords import admin_password, student_password


def add_answers_to_choice(choice, answers):
    for text, score in answers:
        choice.answer_set.create(text=text, score=score)


def create_choice_from_dict(choice_dict, unit):
    choice = exercises.models.Choice(
        is_multiple=choice_dict["is_multiple"],
        unit=unit,
        text=dedent(choice_dict["text"])
    )
    if "image_file_name" in choice_dict:
        choice.image_file_name = choice_dict["image_file_name"]
    choice.save()
    add_answers_to_choice(choice, choice_dict["answers"])
    return choice


def create_code_from_dict(code_dict, unit):
    code = exercises.models.Code(
        unit=unit,
        text=dedent(code_dict["text"]),
        test=code_dict["test"],
    )
    code.save()
    return code


def create_item_from_dict(item_dict, unit):
    type = item_dict.get("type")
    base_url = "http://pyedu.hu/Video/oh/"
    if type in ("nk", "qs", "so"):
        url = item_dict.get(
            "url",
            base_url
        )
        if url == "django":
            url = base_url  # TODO
        unit.video_set.create(
            file_name=item_dict["file_name"],
            url=url,
            type=item_dict.get("type", "nk"),
        )
    elif type is "choice":
        create_choice_from_dict(item_dict, unit)
    elif type is "code":
        create_code_from_dict(item_dict, unit)
    else:
        raise ValueError("There is no item type called {}".format(type))


def create_unit_from_dict(unit_dict, lesson):
    if isinstance(unit_dict, str):
        unit_dict = create_unit_dict_from_string(unit_dict)
    unit = lesson.unit_set.create(
        serial=unit_dict["serial"],
        max_score=unit_dict.get("max_score", 5),
        title=unit_dict.get("title", ""),
        note=unit_dict.get("note", ""),
    )
    items = unit_dict.get("items")
    if items:
        for item_dict in items:
            create_item_from_dict(item_dict, unit)
    return unit


def create_lesson_from_dict(lesson_dict, course):
    lesson = course.lesson_set.create(
        serial=lesson_dict["serial"],
        min_score=lesson_dict.get("min_score", 50),
        title=lesson_dict["title"],
        public=lesson_dict.get("public", False),
    )
    units = lesson_dict.get("units")
    if units:
        for unit_dict in units:
            create_unit_from_dict(unit_dict, lesson)
    return lesson


def create_unit_dict_from_string(unit_string):
    unit_dict = load(unit_string)
    items_list = []
    for key in "nk qs so".split():
        if key in unit_dict:
            file_name = remove_video_extension(unit_dict.pop(key))
            item = dict(file_name=file_name, type=key)
            if "url" in unit_dict:
                item["url"] = unit_dict["url"]
            items_list.append(item)
    if "ex" in unit_dict:
        items_list.append(create_choice_dict_from_gift(unit_dict.pop("ex")))
    unit_dict['items'] = items_list
    return unit_dict


def create_course_from_dict(course_dict):
    course = Course.objects.create(
        title=course_dict["title"],
        description=dedent(course_dict["description"]).strip(),
    )
    lessons = course_dict.get("lessons")
    if lessons:
        for lesson_dict in lessons:
            create_lesson_from_dict(lesson_dict, course)
    return course


def create_courses(course_dict_list):
    for course_dict in course_dict_list:
        create_course_from_dict(course_dict)


masodfoku_unittest = """
import unittest


known_values = (
    ((1, 2, 1), (-1, -1), 0),
    ((1, -2, 1), (1, 1), 0),
    ((1, -1, -6), (3, -2), 25),
    ((1, -5, 6), (3, 2), 1),
)


class MasodfokuTest(unittest.TestCase):

    def test_masodfoku_returns_proper_values(self):
        "A masodfoku függvénynek helyes értékekkel kell visszatérnie."
        for args, return_values, _ in known_values:
            self.assertEqual(set(masodfoku(*args)), set(return_values))

    def test_masodfoku_raise_exception_if_number_of_args_not_3(self):
        "A masodfoku függvény kivételt dob, ha nem három argumentummal hívják."
        bad_args = ((1, 2, 1, 1), (1,), (1, 2))
        for args in bad_args:
            with self.assertRaises(AssertionError):
                masodfoku(*args)


class DiszkriminansTest(unittest.TestCase):

    def test_diszkriminans_returns_proper_values(self):
        "A diszkriminans függvénynek helyes értékekkel kell visszatérnie."
        for args, _, return_values in known_values:
            self.assertEqual(diszkriminans(*args), return_values)


test_cases = [MasodfokuTest, DiszkriminansTest]
"""


formal_languages_course_dict = dict(
    title="Tesztkurzus formális nyelvekről",
    description=kiserleti_kurzus_text + """
    A programozási nyelveket ugyanúgy nyelvtanokkal írhatjuk le,
    mint az emberek által beszélt nyelveket.
    A tantárgyban többek között azzal ismerkedünk meg,
    hogyan írhatók fel ilyen nyelvtanok.
    """,
    lessons=[
        dict(
            serial=2,
            title="Véges automaták",
            units=[
                dict(
                    serial=21,
                    max_score=5,
                    title="Levezetés konfigurációsorozattal",
                    note="This is a note.",
                    items=[
                        dict(type="nk", file_name=test_video_name,),
                        dict(
                            type="choice",
                            is_multiple=False,
                            text="Egy véges automata esetén elképzelhető-e, "
                            "hogy egy szót többféleképpen lehet levezetni "
                            "konfigurációsorozattal?",
                            answers=(
                                ("Igen, ez akár determinisztikus "
                                 "automatánál is előfordulhat.", 0),
                                ("Igen, de csak nemdeterminisztikus "
                                 "automata esetén.", 1),
                                ("Ez nem fordulhat elő.", 0),
                            ),
                        ),
                        dict(type="so", file_name=test_video_name,),
                    ],
                ),
                dict(
                    serial=10,
                    max_score=5,
                    title="A véges automaták kiinduló állapota",
                    items=[
                        dict(type="nk", file_name=test_video_name,),
                    ],
                ),
                dict(
                    serial=19,
                    max_score=5,
                    title="Konfiguráció",
                    items=[
                        dict(type="nk", file_name=test_video_name,),
                    ],
                ),
                dict(
                    serial=1,
                    max_score=5,
                    title="A véges automaták felépítése",
                    items=[
                        dict(type="nk", file_name=test_video_name,),
                    ],
                ),
            ]
        ),
        dict(
            serial=1,
            title="Reguláris kifejezések",
            units=[
                dict(
                    serial=2,
                    max_score=5,
                    title="A 4 alapvető építőelem",
                    items=[
                        dict(type="qs", file_name=test_video_name),
                        dict(
                            type="choice",
                            is_multiple=True,
                            text="Melyik jelnek van speciális"
                            "jelentése a reguláris kifejezések esetén?",
                            answers=(
                                ("*", 0.5),
                                (".", 0.5),
                                ("&", -0.5),
                                ("@", -0.5),
                            ),
                        ),
                        dict(type="so", file_name=test_video_name),
                    ],
                ),
            ]
        ),
        dict(
            serial=5,
            title="Levezetési fa",
            units=[
                dict(
                    serial=2,
                    max_score=5,
                    title="Jobb és baloldali levezetés",
                    note="This is another note.",
                    items=[
                        dict(type="nk", file_name=test_video_name),
                        dict(
                            type="code",
                            text="Írjon egy függvényt, "
                            "amely kiszámolja a másodfokú egyenlet gyökeit!...",
                            test=masodfoku_unittest,
                        ),
                    ],
                ),
            ]
        ),
        dict(
            serial=99,
            title="Üres lecke végén",
        ),
        dict(
            serial=3,
            title="Üres lecke belül",
        ),
    ]
)

formal_languages_item_list = (
    (1, 2, 'qs'),
    (1, 2, 'ex'),
    (1, 2, 'so'),
    (2, 1, 'nk'),
    (2, 10, 'nk'),
    (2, 19, 'nk'),
    (2, 21, 'nk'),
    (2, 21, 'ex'),
    (2, 21, 'so'),
    (5, 2, 'nk'),
    (5, 2, 'ex'),
)


def add_student(username, **kwargs):
    password = kwargs.pop("password", student_password)
    email = kwargs.pop("email", "{}@uni-obuda.hu".format(username))
    first_name = kwargs.pop("first_name", "")
    last_name = kwargs.pop("last_name", "")
    assert not kwargs, "Invalid arguments: {}".format(kwargs.keys())
    assert isinstance(email, str)

    user = auth.models.User.objects.create_user(
        username=username,
        first_name=first_name,
        last_name=last_name,
        password=password,
        email=email)
    students = auth.models.Group.objects.get(name='students')
    students.user_set.add(user)
    # print("Students:", *[s.username for s in students.user_set.all()])
    return User.objects.get(username=username)


def add_students_with_name_and_email(student_list):
    for i, (first_name, last_name, email) in enumerate(student_list, 10):
        add_student(
            username=email,
            password=student_password,
            first_name=first_name,
            last_name=last_name,
            email=email)


def add_students_from_file(filename):
    with open(filename) as f:
        lines = f.readlines()
    student_list = [line.strip().split(",") for line in lines]
    add_students_with_name_and_email(student_list)


def add_anonymous():
    return add_student('anonymous')


def add_admin():
    auth.models.User.objects.create_superuser(
        last_name="Admin",
        username="admin",
        password=admin_password,
        email="admin@uni-obuda.hu")


def add_students(*students):
    return [add_student(st) for st in students]


def create_database(course_dict_list):
    create_courses(course_dict_list)
    add_base_groups()
    add_admin()
    add_anonymous()
    add_students("diak", "diak2")


def main():
    create_database(
        [
            formal_languages_course_dict,
            test_net_course_dict,
            oh_course_dict
        ]
    )


def clean_main():
    create_database([oh_course_dict])
