import os


def delete_database(file_name):
    if os.path.isfile(file_name):
        os.system("rm {}".format(file_name))
        print("Database deleted")


def run():
    from courses.create_course import main

    # delete_database('db.sqlite3')
    print("Drop your database")
    print("PostgreSQL: sudo su - postgres; dropdb pyedu; createdb --owner pyedu pyedu")
    os.system("./manage.py migrate auth")
    os.system("./manage.py migrate courses")
    os.system("./manage.py migrate")

    main()
    print("Database created")


if __name__ == '__main__':
    print('Run this script as\n'
          'manage.py runscript setup_database\n'
          'Install django-extensions before (e.g. with pip3 or pip-3.4)'
          )
