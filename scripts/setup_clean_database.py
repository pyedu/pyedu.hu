import os


def delete_database(file_name):
    if os.path.isfile(file_name):
        os.system("rm {}".format(file_name))
        print("Database deleted")


def run():
    from courses.create_course import clean_main

    delete_database('db.sqlite3')
    os.system("./manage.py migrate")

    clean_main()
    print("Database created")


if __name__ == '__main__':
    print('Run this script as\n'
          'manage.py runscript setup_clean_database\n'
          'Install django-extensions before (e.g. with pip3 or pip-3.4)'
          )
