pyEdu website (django) project
================================

The goal of pyEdu is to serve online courses like Udacity do.

Requirements and installation notes
--------------------------------------

pyEdu was tested using django 1.11.

You need to install [pygiftparser from github](https://github.com/horvatha/pygiftparser)

All the other third party modules can be installed with pip (see
requirements.txt).

Arpad Horvath [e-mail](https://pyedu.hu/fepy/img/email.png) [e-mail](http://harp.pythonanywhere.com/static/humor/email.png) 

You need to set passwords if you use the scripts to create courses.
Create a file courses/passwords.py with the text like this:

    student_password = "Student3Password"
    admin_password = "Admin43Password"

If you have some users with first_name, last_name and e-mail address in a
file separated with comma, you can create those users with the commands
like this in (i)python shell::

    from courses import create_course
    create_course.add_students_from_file("/home/myname/name_email_last_firstname.csv")

You can get a csv file like this from a Moodle course. Import the data
of participants as LibreOffice or Excel table. Delete the other columns
with LibreOffice or Excel and save it as a CSV file.
